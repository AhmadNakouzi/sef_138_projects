<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'name' => str_random(10),
            'phone_number' => str_random(15),
            'prof_pic_path' => str_random(10),
            'status' => str_random(10),
        ]);
    }
}