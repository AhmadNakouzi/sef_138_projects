<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liker extends Model
{
    protected $table = 'post_likers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userID', 'postID',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
