var loader = document.getElementById('loader');

window.onload = function () {
    //initialize the url content
    document.getElementById("blogURL").value = "https://medium.com/s/story/how-compassion-guided-me-up-the-mountains-of-india-e4e7895a671d";
    document.getElementById("blogSummarize").addEventListener("click", function () {
        retrieveBlogContent();
    });
};

function retrieveBlogContent() {
    var blogURL = document.getElementById("blogURL").value;
    var xhttp = new XMLHttpRequest();

    loader.style.display = "block";
    xhttp.open("GET", "parse.php?type=getWebContent&blogURL=" + blogURL, true);
    xhttp.send();

    xhttp.onreadystatechange = function () {
        // readyState : 4 ==> request finished and response is ready
        // status = 200 ==> 'OK'
        if (this.status === 200 && this.readyState === 4) {
            //DOMParser provides the ability to parse html or xml from string
            var html = new DOMParser().parseFromString(this.responseText, "text/html");
            var title = html.getElementsByTagName("h1")[0].textContent;
            var paragraph = html.getElementsByClassName('section-inner');
            var parContent = '';

            for (var j = 0; j < paragraph.length; j++) {
                for (var i = 0; i < paragraph[j].childElementCount; i++) {
                    if (paragraph[j].childNodes[i].tagName === "P") {
                        parContent += paragraph[j].childNodes[i].textContent;
                    }
                }
            }
            blogSummarize(title, parContent);
        }
    };
}

function blogSummarize(title, text) {

    var data = "title=" + title + "&text=" + text;
    var xhttp = new XMLHttpRequest();

    xhttp.open("POST", "parse.php", true);
    //Send the proper header information along with the request
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(data);
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            var summarizedBlog = JSON.parse(xhttp.responseText);
            var summarizedBlogs = document.getElementById('summarizedBlogs');
            loader.style.display = "none";

            var p = document.createElement('p');

            for (var i =0;i<summarizedBlog.sentences.length ;i++)
            {
                p.innerHTML +=summarizedBlog.sentences[i];
            }
            summarizedBlogs.innerHTML = '';
            summarizedBlogs.appendChild(p);
        }
    };
}