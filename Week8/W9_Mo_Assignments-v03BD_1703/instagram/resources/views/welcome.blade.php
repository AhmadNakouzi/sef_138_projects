<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/instaStyle.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<article id="homePage">
    <div>
        <img id="instaMobile" src="{{asset('images/insta-logo.png')}}">
    </div>
    <div id="intagramRegistration">
        @if (Route::has('login'))
            <div class="links">
                <h1 class="instaWords">Instagram</h1>
                <h2 class="instaWords">Sign up to see photos and videos from your friends.</h2>
                <button class="button">
                    <i class="fa fa-facebook-official"></i>
                    Log in with Facebook
                </button>
                {{--<div class="separator-left margin-bottom"></div>--}}
                <div class="separator"> OR</div>
                {{--<div class="separator-right margin-bottom"></div>--}}
                <form>
                    <input type="text" placeholder="Mobile Number or Email" class="registration-input"/>
                    <input type="text" placeholder="Full Name" class="registration-input"/>
                    <input type="text" placeholder="Username" class="registration-input"/>
                    <input type="password" placeholder="Password" class="registration-input"/>
                    <button class="button" type="submit" href="{{ route('register') }}">Sign up</button>
                    <p class="instaWords">By signing up, you agree to our Terms &amp; Privacy Policy</p>
                </form>
            </div>
        <div class="clear"></div>
            <div id="intagramLogin">
                {{--@auth--}}
                    {{--<a href="{{ url('/home') }}">Home</a>--}}
                {{--@else--}}
                <p id="instaLoginWords">
                   Have an account? <a href="{{ route('login') }}">Log in</a>
                </p>
                    {{--<a href="{{ route('register') }}">Register</a>--}}
                {{--@endauth--}}
            </div>
        @endif
    </div>
</article>
</body>
</html>
