<?php

use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIDs = DB::table('users')->select('id')->first();

        DB::table('channels')->insert([
            'name' => str_random(10),
            'purpose' => str_random(15),
            'admin_id' => rand(1, $usersIDs->id),
        ]);
    }
}