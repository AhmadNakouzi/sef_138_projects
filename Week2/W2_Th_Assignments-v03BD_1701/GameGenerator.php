<?php 
class GameGenerator 
{

	public function PlayGame($nbOfGames)
	{
		// Initializing big and small cards
		$bigChoices = array('25', '50', '75', '100');
		$smallChoices = array('1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '7', '7', '8', '8', '9', '9', '10', '10');
		
		for ($counter = 0; $counter < $nbOfGames; $counter++)
		{
			$randBigNumber = rand(1,4);//Choosing random numbers from 1 to 4 maximum
			$choosenRandNumbers = array();

			if ($randBigNumber == 0) {
				$randBigkeys = (array) array_rand($bigChoices, $randBigNumber + 1);//Getting the numbers from big choices
			} else {
				$randBigkeys = (array) array_rand($bigChoices, $randBigNumber);//Getting the numbers from big choices
			}
			for ($i = 0; $i < sizeof($randBigkeys) ; $i++)
			{
				array_push($choosenRandNumbers, $bigChoices[$randBigkeys[$i]]);//Pushing the numbers of big choices into the choosenRandNumbers array
			}
			$totalRemaining = 6 - $randBigNumber;
			$randSmallkeys = array_rand($smallChoices, $totalRemaining);//Getting the numbers from small choices
			
			for ($i=0; $i < $totalRemaining; $i++)
			{ 
				array_push($choosenRandNumbers, $smallChoices[$randSmallkeys[$i]]);//Pushing the numbers of small choices into the choosenRandNumbers array
			}
			$target = rand(101,999);//Choose random number between 101 and 999 as a target to reach
			rsort($choosenRandNumbers);
			$possibileNumbers = new Posibilites();
			$possibileNumbers = $possibileNumbers->Permutation($choosenRandNumbers);
			$operators = array('*','+','-','/');
			$possibileOperators = new Posibilites();
			$possibileOperators = $possibileOperators->Permutation($operators);
			$solver = new GameSolver($counter + 1, $choosenRandNumbers, $possibileNumbers, $possibileOperators, $target);
		}
	}
}
?>