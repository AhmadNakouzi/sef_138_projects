function addComment() {
    var comment = $('textarea[name=comment]').val();
    var postID = ($('textarea[name=comment]').parent().parent())[0].id;

    event.preventDefault();

    if (comment !== '') {
        $.ajax({
            contentType: 'application/json',
            method: 'post',
            data: JSON.stringify({
                _token: $('input[name=_token]').val(),
                comment: comment,
                postID: postID,
            }),
            url: 'addComment',
        }).done(function (data) {
            if ((data.errors)) {
                $('.error').removeClass('hidden');
                $('.error').text(data.errors.name);
            } else {
                $('#postComments').append("<p>" + comment + "</p>");
                $('textarea[name=comment]').val('');
            }
        });
    }
}

$("newComment").click(function(e) {
    $('textarea[name=comment]').focus();
});