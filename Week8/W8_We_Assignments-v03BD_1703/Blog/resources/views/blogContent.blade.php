@extends('layouts.app')
@section('content')
    @if(isset($blogContent))
        <div class="mx-auto mt-2 rounded border-secondary border w-50 p-2"
             @if(isset($blogContent->id))
             id="blog{{$blogContent->id}}"
                @endif>
            <h2> {{$blogContent->title}}</h2>
            <h4> {{$blogContent->description}}</h4>
            <h6>Posted by: {{$blogContent->id }}</h6>
        </div>
    @endif
@endsection