use sakila;
SELECT lang.name
FROM language AS lang , film AS F
WHERE release_year = '2006'
AND F.language_id = lang.language_id
GROUP BY F.language_id
ORDER BY F.language_id DESC
LIMIT 3;
