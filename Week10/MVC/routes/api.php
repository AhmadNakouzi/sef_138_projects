<?php

class Routes
{
	function get()
	{
		$routes = [
			'film' => [
				'read' => [
					'method' => 'get',
					'uses' => 'FilmsController@index'
				],
				'create' => [
					'method' => 'post',
					'uses' => 'FilmsController@create'
				],
				'update' => [
					'method' => 'patch',
					'uses' => 'FilmsController@update'
				],
				'delete' => [
					'method' => 'delete',
					'uses' => 'FilmsController@destroy'
				]
			],

			'customer' => [
				'read' => [
					'method' => 'get',
					'uses' => 'CustomersController@index'
				],
				'create' => [
					'method' => 'post',
					'uses' => 'CustomersController@create'
				],
				'update' => [
					'method' => 'patch',
					'uses' => 'CustomersController@update'
				],
				'delete' => [
					'method' => 'delete',
					'uses' => 'CustomersController@destroy'
				]
			],

			'actor' => [
				'read' => [
					'method' => 'get',
					'uses' => 'ActorsController@index'
				],
				'create' => [
					'method' => 'post',
					'uses' => 'ActorsController@create'
				],
				'update' => [
					'method' => 'patch',
					'uses' => 'ActorsController@update'
				]
				// ],
				// 'delete' => [
				// 	'method' => 'delete',
				// 	'uses' => 'ActorsController@destroy'
				// ]
			],

			'address' => [
				'read' => [
					'method' => 'get',
					'uses' => 'AddressesController@index'
				],
				'create' => [
					'method' => 'post',
					'uses' => 'AddressesController@create'
				],
				'update' => [
					'method' => 'patch',
					'uses' => 'AddressesController@update'
				],
				'delete' => [
					'method' => 'delete',
					'uses' => 'AddressesController@destroy'
				]
			],
		];
	return $routes;
	}
}