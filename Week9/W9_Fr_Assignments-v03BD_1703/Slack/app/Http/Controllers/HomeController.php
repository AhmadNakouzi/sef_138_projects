<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Message;
use Illuminate\Support\Facades\Redirect;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function welcome()
    {
        return view('welcome');
    }

    /**
     * Show the Application home page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $userMembership = [];
        $user = auth()->user();
        $members = $user->member()->where('user_id', $user->id)->pluck('channel_id');
        if ($members->count() !== 0) {
            $conversations = Message::where('channel_id', $members)->get();
            $channelAdmin = Channel::where('admin_id', $user->id)->pluck('id');

            foreach ($members as $member) {
                $channels = Channel::where('id', $member)->select('id', 'name')->get();
                array_push($userMembership, $channels);
            }
            return view('home')->with(['channels' => $userMembership, 'conversations' => $conversations, 'channelAdmin' => $channelAdmin]);
        } else {
            $error = 'You are not connected to any channel yet.';
            return view('home')->with(['error' => $error]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getChannel(Request $request)
    {
        return $this->Home($request->name);
    }

    /**
     * Store chat messages in tables messages with channel id and user id
     * @param Request $request
     */
    public function saveChat(Request $request)
    {
        echo $request;
        $user = auth()->user()->id;
        Message::create([
            'message' => $request->input('msg'),
            'user_id' => $user,
            'channel_id' => 1]);
    }

    /**
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Home($name)
    {
        $channelID = Channel::where('name', $name)->pluck('id');
        $userMembership = [];
        $user = auth()->user();
        $members = $user->member()->where('user_id', $user->id)->pluck('channel_id');
        $conversations = Message::where('channel_id', $channelID)->get();
        $channelAdmin = Channel::where('admin_id', $user->id)->pluck('id');

        foreach ($members as $member) {
            $channels = Channel::where('id', $member)->select('id', 'name')->get();
            array_push($userMembership, $channels);
        }

        return view('home')->with(['choosenChannel' => $name, 'channels' => $userMembership, 'conversations' => $conversations, 'channelAdmin' => $channelAdmin]);
    }

    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    public function HomeWithError($error)
    {
        $userMembership = [];
        $user = auth()->user();
        $members = $user->member()->where('user_id', $user->id)->pluck('channel_id');
        $conversations = Message::where('channel_id', 1)->get();
        $channelAdmin = Channel::where('admin_id', $user->id)->pluck('id');

        foreach ($members as $member) {
            $channels = Channel::where('id', $member)->select('id', 'name')->get();
            array_push($userMembership, $channels);
        }

        return Redirect::to('home')->with(['channels' => $userMembership, 'conversations' => $conversations, 'channelAdmin' => $channelAdmin, 'error' => $error]);
    }
}
