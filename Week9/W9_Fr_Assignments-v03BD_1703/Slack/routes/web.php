<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('/','HomeController@welcome');

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home/{name}', 'HomeController@getChannel')->name('home');
    Route::post('/home', 'ChannelController@newChannel')->name('newChannel');
    Route::get('/channel', 'ChannelController@getChannel')->name('channel');
    Route::post('/saveChat', 'HomeController@saveChat')->name('saveChat');
    Route::post('/addMember/{id}', 'ChannelController@newMember')->name('addMember')->where('id', '^([0-9])');
});