"use strict";
var myTimer = null;
var moveInfo;
var moveInc = 1;

var callStack;

var barsInfo = [{}, {}, {}];

var diskPosTop, diskPosLeft, DiskID;

//window.onload will execute functions or event once the window is firstly loading
window.onload = function ()
{
    //get disks by id and assign to variables to organize the movement of them
    var disk0 = document.getElementById('disk0');
    var disk1 = document.getElementById('disk1');
    var disk2 = document.getElementById('disk2');
    var disk3 = document.getElementById('disk3');
    var disk4 = document.getElementById('disk4');
    var disk5 = document.getElementById('disk5');
    var disk6 = document.getElementById('disk6');
    var disk7 = document.getElementById('disk7');

    //declare 3 stacks to get the top element on each movement
    DiskID = [disk0, disk1, disk2, disk3, disk4, disk5, disk6, disk7];
    diskPosTop = [];
    diskPosLeft = [];

    //init the top and left of each disk
    for (var i = 0; i < 8; i++) {
        diskPosTop[i] = DiskID[i].style.top;
        diskPosLeft[i] = DiskID[i].style.left;
    }
};

function executeHanoi() {
    var diskCount = 8;
    // Move Disks to start column if it's the second time we play
    for (var i = 0; i < diskCount; i++) {
        DiskID[i].style.top = diskPosTop[i];
        DiskID[i].style.left = diskPosLeft[i];
    }

    barsInfo[0].disks = ['disk0', 'disk1', 'disk2', 'disk3', 'disk4', 'disk5', 'disk6', 'disk7'];
    barsInfo[1].disks = [];
    barsInfo[2].disks = [];

    callStack = [];  // callStack array is global

    Hanoi(diskCount, 0, 2, 1);

    moveDisk(); // moveDisk takes its parameters from callStack
}

//tower of hanoi algorithm
function Hanoi(n, from, to, via) {
    if (n === 0)
        return;
    Hanoi(n - 1, from, via, to);
    // moveDisk(from,to);

    callStack.push([from, to]); // save parameters to callStack array
    Hanoi(n - 1, via, to, from);

}

function moveDisk() {
    if (callStack.length === 0)
        return;

    var param = callStack.shift();  // Get first element from callStack and assign it to variable
    var fromBar = param[0];
    var toBar = param[1];

    var elem = document.getElementById(barsInfo[fromBar].disks.pop());  // find top elemnet in fromBar

    moveInfo = {
        elem: elem,
        fromBar: fromBar,
        toBar: toBar,
        whichPos: "top", // element position property for movement
        dir: -1,  // 1 or -1
        state: "up", // move upward
        endPos: 60    // end position (in pixels) for move upward
    };
    myTimer = setInterval(animateMove, 0); // Start animation
}

function animateMove() {
    var elem = moveInfo.elem;
    var dir = moveInfo.dir;

    var pos = parseInt(elem[(moveInfo.whichPos === "left") ? "offsetLeft" : "offsetTop"]);

    if (((dir === 1) && (pos >= moveInfo.endPos)) || ((dir === -1) && (pos <= moveInfo.endPos))) {  // alert(moveInfo.state);
        if (moveInfo.state === "up") {
            moveInfo.state = "hor";
            moveInfo.whichPos = "left";
            moveInfo.dir = 1;
            if (moveInfo.fromBar > moveInfo.toBar) moveInfo.dir = -1;
            //alert("toBar:" + moveInfo.toBar);
            toBar = document.getElementById("bar" + moveInfo.toBar);
            // Next line: 15px is half of tower width
            moveInfo.endPos = toBar.offsetLeft - Math.floor(elem.offsetWidth / 2) + 15;
            return;
        }

        else if (moveInfo.state === "hor") // move down
        {
            moveInfo.state = "down";
            moveInfo.whichPos = "top";
            moveInfo.dir = 1;
            //alert(elem.offsetHeight);
            moveInfo.endPos = document.getElementById("bottombar").offsetTop - (barsInfo[moveInfo.toBar].disks.length + 1) * elem.offsetHeight;
            return;
        }

        else // end of current call to moveDisk, issue next call
        {
            clearInterval(myTimer);  // cancel timer
            barsInfo[moveInfo.toBar].disks.push(elem.id);
            moveDisk();
            return;
        }
    }


    // Move Disk
    pos = pos + dir * moveInc;
    elem.style[moveInfo.whichPos] = pos + "px";

    // Move the inside middle image
    if (moveInfo.state === "up") {
        var fromBar = document.getElementById("bar" + moveInfo.fromBar);
        if (elem.offsetTop < fromBar.offsetTop) {
            var y = elem.getElementsByClassName("insideImg")[0].offsetHeight;
            if (y > 0) elem.getElementsByClassName("insideImg")[0].style.height = y - moveInc + "px";
        }
    }

    if (moveInfo.state === "down") {
        var toBar = document.getElementById("bar" + moveInfo.toBar);
        if (elem.offsetTop > toBar.offsetTop) {
            y = elem.getElementsByClassName("insideImg")[0].offsetHeight;
            if (y < 14) elem.getElementsByClassName("insideImg")[0].style.height = y + moveInc + "px";
        }
    }

}