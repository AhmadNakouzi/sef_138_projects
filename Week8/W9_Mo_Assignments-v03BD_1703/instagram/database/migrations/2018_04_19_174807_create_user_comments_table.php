<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_comments', function (Blueprint $table) {
            $table->increments('userCommentsID');
            $table->text('commentText');
            $table->unsignedInteger('userID');
            $table->unsignedInteger('postID');
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('postID')->references('postID')->on('user_posts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_comments');
    }
}
