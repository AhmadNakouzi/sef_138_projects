@extends('layouts.app')
@section('content')
    <form class="form-group w-50 m-auto border-dark mb" method="post">
        {{--action="{{ route("timeline.post") }}">--}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="comment">Blog:</label>
        <br>
        <input id="blogTitle" name="title" type="text" placeholder="Blog title" class="w-100 mb-2 p-2 rounded" required>
        <textarea id="blogDescription" name="description" class="form-control mb-2 rounded noresize" rows="3"
                  placeholder="New blog" required></textarea>
        <button type="submit" class="btn btn-secondary mb-5">Create new blog</button>
    </form>
@endsection