#!/usr/bin/php
<?php
$input = getopt("a:e:t:d:s:m:");
$result = null;

if (isset($input["a"])) {
	$author = $input["a"];
}
if (isset($input["e"])) {
	$email = $input["e"];
}
if (isset($input["t"])) {
	$time = $input["t"];
}
if (isset($input["d"])) {
	$date = $input["d"];
}
if (isset($input["s"])) {
	$timeStamp = $input["s"];
}
if (isset($input["m"])) {
	$message = $input["m"];
}
try{
	if (isset($author) && $author != null && !isset($email)) {
		SearchByAuthorOrEmail($author,null,$result);//Searching by Author
	} elseif (isset($email) && $email != null && !isset($author)) {
		SearchByAuthorOrEmail(null,$email,$result);//Searching by Email
	}
	if (isset($time) && $time != null && isset($date) && $date != null) {
		SearchByDateTime($date,$time,$result);
	} elseif (isset($time) && $time != null && !isset($date)) {
		SearchByDateTime(null,$time,$result);
	} elseif (isset($date) && $date != null && !isset($time)) {
		SearchByDateTime($date,null,$result);
	}
	if (isset($timeStamp) && $timeStamp != null) {

	}
	if (isset($message) && $message != null) {
		SearchByMessage($message,$result);//search for a specific commit
	}
	if(isset($result) && $result != null){
		print_r(shell_exec($result));
	}
}
catch(Exception $e){
	echo "something went wrong";
}
function SearchByAuthorOrEmail($auth, $email, $result)
{
	global $result;
	try {
		if ($auth != null) {
			if ($result == null) {
				$result = "git log --author=$auth";
			} else {
				$result = $result . ' ' . "--author=$auth";
			}
		} elseif ($email != null){
			if ($result == null) {
				$result = "git log --author=$email";	
			} else {
				$result = $result . ' ' . "--author=$email";
			}
		}
	} catch (Exception $e){
		echo "No commits\n";
	}
	return $result;
}
function SearchByDateTime($date, $time, $result)
{
	global $result;
	try {
		if (isset($date) && $date != null && !isset($time)) {
			if ($result == null) {
				$result = "git log --pretty=format:'%H - %an - %ad ' --date=format:' %a %b %d %I:%M:%S %Y %z' --before='$date'" . "'23:59' master";
			} else {
				$result = $result . ' ' . " --pretty=format:'%H - %an - %ad ' --date=format:' %a %b %d %I:%M:%S %Y %z' --before='$date'" . "'23:59' master";
			}
		} elseif (isset($time) && $time != null && !isset($date)) {
			if ($result == null) {//time filtering is not working
				$result = "git log --before='$time'  master";
			} else {
				$result = $result . ' ' . " --before='$time'  master";
			}
		} else{
			echo "under construction\n";
		}
	} catch (Exception $e) {
		echo "No commits \n";
	}
	return $result;	 
}
function SearchByMessage($message, $result)
{
	global $result;
	try {
		if ($result == null) {
			$result = "git log -g --grep=^$message";	
		} else {
			$result = $result . ' ' . "-g --grep=^$message";
		}
	} catch (Exception $e) {
		echo "No commits \n";
	}
	return $result;
}
?>