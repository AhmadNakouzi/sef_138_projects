use sakila;
SELECT C.first_name AS 'First Name', C.last_name AS 'Last Name'
FROM customer AS C,  rental AS R
WHERE C.customer_id = R.customer_id
AND rental_date = '@given_date'
GROUP BY C.customer_id
HAVING  COUNT(R.rental_id) IN 
(
SELECT COUNT(R.rental_id) AS Counted
FROM rental AS R
GROUP BY R.customer_id
ORDER BY Counted DESC
)
LIMIT 3;
