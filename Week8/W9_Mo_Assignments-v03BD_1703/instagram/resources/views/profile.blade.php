@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="w-100">
                <div id="errorMessage" class="text-danger">{{$error or ''}}</div>
                <div class="card">
                    <div class="card-body col-12 w-100">
                        <h3 class="col-12 text-center border-bottom border-dark">{{$name or ''}}
                            <a href="{{route('profile.show')}}">
                                <i class="col-2 fa fa-cog" aria-hidden="true"></i>
                            </a>
                        </h3>
                        <a href="{{ route('editProfilePicture.show') }}">
                            <img class="rounded-circle float-left col-6"
                                 alt="Profile picture"
                                 src="{{$picture or 'storage/images/defualt-logo.jpg'}}"
                                 style="width: 120px; height: 100px;">
                        </a>
                        <div class="float-left col-3 text-center">
                            <h5 class="m-0">{{$countPosts or ''}}</h5>
                            <p>posts</p>
                        </div>
                        <div class="float-left col-3 text-center">
                            <h5 class="m-0">{{$followers or 0}}</h5>
                            <p>followers</p>
                        </div>
                        <div class="float-left col-3 text-center">
                            <h5 class="m-0">{{$following or 0}}</h5>
                            <p>following</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="w-100">
                @if(isset($userPosts))
                    <div class="row col-12">
                        @foreach($userPosts as $post)
                            <div class="col-4 mb-2 mt-5">
                                <img src="{{$post->postImage or '#'}}"
                                     class="w-100"
                                     height="300px">
                            </div>
                        @endforeach
                    </div>
            </div>
            @endif
        </div>
    </div>
    </div>
@endsection