@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Change Profile Picture</div>
                    <div class="card-body">
                        <div id="errorMessage" class="text-danger">{{$error or ''}}</div>
                        <form enctype="multipart/form-data"
                              action="{{route('editProfilePicture.upload')}}"
                              method="post">
                            @csrf
                            <input type="file" id="picture" name="picture" class="mb-3" required>
                            <div class="clear margin-bottom"></div>
                            <input class="btn bg-primary" type="submit" value="Upload">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection