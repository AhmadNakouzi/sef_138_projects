<?php

require_once("config.php");
// require_once("sakilaManager.php");

class FilmsController
{
	private $host = '';
 	private $username = '';
 	private $password = '';
 	private $db_name = '';
 	private $db = '';

 	/**
 	*Initialize all database variables
 	*/
 	public function __construct()
 	{
 		$this->host = HOST;
	 	$this->username = USERNAME;
	 	$this->password = PASSWORD;
	 	$this->db_name = DB_NAME;
 	}
 	
 	/**
 	*Connect to Sakila Database
 	*/
	public function Connect()
 	{
		$this->db = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
 	}

	/**
 	*Close the database connection
 	*/
 	public function Close()
 	{
 		mysqli_close($this->db);
 	}

	/**
 	*Retrieve all films from database 
 	*/
	function index()
	{
		// Check connection
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
		} 

		$film = "SELECT * FROM film;";
		$result = mysqli_query($this->db, $film);

		if ($result->num_rows > 0) {
			$films = [];
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		    	array_push($films, $row);
		    }
		    $json = json_encode($films, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
			print_r($json);
		} else {
		    $error->message = '0 records founded';
			$message = json_encode($error);
			print($message);
		}
		$this->Close();
	}

	/**
	*Create a new film in the database
	*/
	function create()
	{
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
    	} else {
    		if (isset($_POST['title']) && isset($_POST['description'])
  				&& isset($_POST['release_year']) && isset($_POST['language'])
  				&& isset($_POST['original_language']) && isset($_POST['rental_duration'])
  				&& isset($_POST['rental_rate']) && isset($_POST['length'])
  				&& isset($_POST['replacement_cost']) && isset($_POST['rating'])
  				&& isset($_POST['special_features'])) {

  					$title = $_POST['title'];
  					$description = $_POST['description'];
  					$release_year = $_POST['release_year'];
  					$language = $_POST['language'];
  					$original_language = $_POST['original_language'];
  					$rental_duration = $_POST['rental_duration'];
  					$rental_rate = $_POST['rental_rate'];
  					$length = $_POST['length'];
  					$replacement_cost = $_POST['replacement_cost'];
  					$rating = $_POST['rating'];
  					$special_features = $_POST['special_features'];

  					$language_id = $this->validateLanguage($language);
  					$original_language_id = $this->validateLanguage($original_language);
					
  					if (isset($language_id) && isset($original_language_id)) {
  						$newFilm = "INSERT INTO film(title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features) VALUES('$title', '$description', $release_year, $language_id , $original_language_id , $rental_duration, $rental_rate, $length , $replacement_cost , '$rating', '$special_features');";
  						try {
			  				$result = mysqli_query($this->db, $newFilm);
			  				if ($result) {
			  					print('New film added');	
			  				} else {
			  					$error->message = 'Error adding new film';
								$message = json_encode($error);
								print($message);
			  				}
			  				$this->Close();
			  			} catch (MySQLDuplicateKeyException $e) {
			  				$error->message = $e->getMessage();
							$message = json_encode($error);
							print($message);
			  			}
  					}
	    		}  else {
	    			$error->message = 'Incorrect parameters';
					$message = json_encode($error);
					print($message);
				} 
		}
	}

	/**
	*Validates if city exists in database
	*/
	function validateLanguage($language)
	{
		$language = "SELECT language_id from language WHERE name = '" . $language . "'";
		$language = mysqli_query($this->db, $language);
		$language = mysqli_fetch_assoc($language);
		
		return $language['language_id'];
	}
}