@extends('layouts.app')
@section('content')

    @if(isset($myBlogs))
        @foreach($myBlogs as $blog)
            <div class="mx-auto mt-2  card w-50 p-2"
                 @if(isset($blog->id))
                 id="blog{{$blog->id}}"
                    @endif>
                <h2> {{$blog->title}}</h2>
                <h4> {{$blog->description}}</h4>
                <a @if(isset($blog->id)) class="btn btn-danger w-25 align-self-center mb-2"
                   href="{{route('removeMyBlog',['id' => $blog->id])}}"
                        @endif>Remove {{$blog->title}}</a>
                <a @if(isset($blog->id)) class="btn btn-secondary w-25 align-self-center"
                   href="{{route('editMyBlog',['id' => $blog->id])}}"
                        @endif>Edit {{$blog->title}}</a>

            </div>
        @endforeach
        @if(isset($myBlogs))
            <div class="mx-auto">
                {{ $myBlogs->links() }}
            </div>
        @endif
    @endif
    @if(session('error'))
        <label>{{ session('error') }}</label>
    @endif

@endsection