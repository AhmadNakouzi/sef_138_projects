@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Upload a new Image</div>
                    <div class="card-body">
                        <div id="errorMessage" class="text-danger">{{$error or ''}}</div>
                        <div id="successMessage" class="text-success">{{$success or ''}}</div>
                        <form enctype="multipart/form-data"
                              action="{{route('post.upload')}}"
                              method="post">
                            @csrf
                            <input type="file" id="picture" name="picture" class="mb-3" required>
                            <div class="clear margin-bottom"></div>
                            <textarea id="caption" name="caption" rows="5" cols="50" placeholder="caption" class="p-2"></textarea>
                            <div class="clear mb-2"></div>
                            <input class="btn bg-primary" type="submit" value="Post">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection