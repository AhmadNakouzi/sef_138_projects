<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIDs = DB::table('users')->select('id')->first();
        $channelIDs = DB::table('channels')->select('id')->first();

        DB::table('members')->insert([
            'user_id' => rand(1, $usersIDs->id),
            'channel_id' => rand(1, $channelIDs->id),
        ]);
    }
}
