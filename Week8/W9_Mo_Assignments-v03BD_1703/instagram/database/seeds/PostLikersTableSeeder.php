<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class PostLikersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIDs = DB::table('users')->select('id')->first();
        $postIDs = DB::table('user_posts')->select('postID')->first();

        DB::table('post_likers')->insert([
            'userID' => rand(1, $usersIDs->id),
            'postID' => rand(1, $postIDs->postID),
        ]);
    }
}
