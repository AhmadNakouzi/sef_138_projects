<?php
    require_once("MySQLWrapper.php");
    session_start();
    //Get the email, film and date from order page form
	$email = trim($_POST['email']);
    $film = $_POST['ddlMovie'];
    $dateTo = $_POST['dateTo'];
    //save email in session to be used another time 
    $_SESSION["email"] = $email;

    if ($email == '' )
    {
        echo '<script> alert("Please fill required fields"); </script>';
        echo '<script>window.location="Order.php";</script>';
    }
    else if ( strtotime($dateTo) < strtotime(date("Y-m-d h:m:sa")))
    {
        echo '<script> alert("Duration to can not be before today"); </script>';   
        echo '<script>window.location="Order.php";</script>';
    }
    else
    {
        try
        {
            //initialize the database host, username, password, and database Name
            $connection = new SakilaDb();
            //connect to database
            $connection->Connect();
            //returns customer ID and store ID
            $custID = $connection->GetEmailInfo($email);
            $customerID = $custID['customer_id'];
            $storeID = $custID['store_id'];         
            $inventoryID = $connection->GetInventoryID($film,$storeID);
            //retreive the staff id to be inserted in Rental table
            $staffID = $connection->GetStaffID($customerID);
            //insert into rental
            $connection->InsertNewRental($dateFrom, $inventoryID , $custID, $dateTo, $staffID);
            //close the database connection
            $connection->Close();
            echo '<script> alert("You have rented the film successfully"); </script>';
            echo '<script>window.location="Order.php";</script>';
        } 
        catch (Exception $e)
        {
            echo '<script> alert("Fail to connect to database"); </script>';   
            echo '<script>window.location="Order.php";</script>'; 

        } 
    }
    // header('Location: Order.php');
?>