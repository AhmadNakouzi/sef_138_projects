@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-danger ">Statistics</h1>
        <div class="row justify-content-center">

            <div class="col-md-3 mr-5 bg-primary text-center rounded">
                <p class="dashStat text-light">My Blogs</p>
                <h3>{{$ownBlogs or 0}}</h3>
            </div>

            <div class="col-md-3 mr-5 bg-success text-center rounded">
                <p class="dashStat text-light">Users Blogs</p>
                <h3>{{$nbBlogs or 0}}</h3>
            </div>
        </div>
@endsection