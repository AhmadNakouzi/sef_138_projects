<?php
	require_once 'MySQLWrapper.php';	
	session_start();  
?>
<!DOCTYPE html>
<html>
<head>
	<title>Rental Store</title>
	<link rel="stylesheet" href="css/myStyle.css">
</head>
<body>
    <div class="bgDiv">
    	<p>------ DVD MALL ------</p>
		<form class="form" method="post" action="OrderProcess.php">
			Email: &nbsp; <input type="email" id="email" name="email" class="email" 
				value="<?php if (isset($_SESSION["email"])) echo $_SESSION["email"];?>" required/><br/>
			Movies: 
			<select id="ddlMovie" name="ddlMovie" class="ddlMovie">
				<?php 
					$connection = new SakilaDb();
			        $connection->Connect();
			        $moivesToken = $connection->GetMovies();
			        if ($moivesToken)
			        {
			        	while ($row = $moivesToken->fetch_assoc())
			        	{
				        	echo "<option value=" . $row["film_id"] . ">" . $row["title"] . "</option>";
				        }
			    	}
			    	else
			    	{
			    		echo "<option>Sorry but all movies are rented </option>";
			    	}
			    	$connection->Close();
			    ?>
			</select>
			<section>
				<p class="durTitle">Rental duration</p>
				<p class="durTitle">------------------------------</p>
				To: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="datetime-local" id="dateTo" name="dateTo" class="durDate" required>
			</section>
			<input type="submit" id="submit" name="submit" value="Rent" class="btnSubmit">
			<button type="reset" value="Reset" class="btnReset">Reset</button>
		</form>
	</div>
</body>
</html>
