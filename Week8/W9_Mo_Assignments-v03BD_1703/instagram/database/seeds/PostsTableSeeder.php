<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIDs = DB::table('users')->select('id')->first();

        DB::table('user_posts')->insert([
            'postImage' => str_random(10),
            'caption' => str_random(10),
            'userID' => rand(1, $usersIDs->id),
        ]);
    }
}
