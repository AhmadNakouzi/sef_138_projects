<?php

$blogURL = $_GET['blogURL'];
$title = $_POST['title'];
$text = $_POST['text'];

if (!extension_loaded('curl')) {
    echo 'failed';
} else {
    try {
        if ($blogURL) {
            $HTTP_Request = curl_init();
            curl_setopt($HTTP_Request, CURLOPT_URL, $blogURL);
            curl_setopt($HTTP_Request, CURLOPT_RETURNTRANSFER, 1);

            echo curl_exec($HTTP_Request);

            curl_close($HTTP_Request);

        } elseif ($title !== null && $text !== null) {

            $targetURL = 'https://api.aylien.com/api/v1/summarize';
            $userAuth = array('X-AYLIEN-TextAPI-Application-ID:' . '5cf81d7b',
                'X-AYLIEN-TextAPI-Application-Key:' . 'e35e070f2e52675a4c45103b263f1f48');

            $parameters = array(
                'url' => 'https://medium.com/s/story/how-compassion-guided-me-up-the-mountains-of-india-e4e7895a671d',
                'title' => $title,
                'text' => $text);

            $HTTP_Request = curl_init($targetURL);
//            curl_setopt($HTTP_Request, CURLOPT_URL, $targetURL);
            curl_setopt($HTTP_Request, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($HTTP_Request, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($HTTP_Request, CURLOPT_HTTPHEADER, $userAuth);
            curl_setopt($HTTP_Request, CURLOPT_POSTFIELDS,
                preg_replace("/%5B[0-9]+%5D=/i", "=", http_build_query($parameters)));
            echo curl_exec($HTTP_Request);
            curl_close($HTTP_Request);
        } else {
            print_r($_POST);
        }
    }
    catch (Exception $exception)
    {
        echo $exception->getMessage();
    }
}