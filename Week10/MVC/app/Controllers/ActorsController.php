<?php

require_once("config.php");

class ActorsController
{
	private $host = '';
 	private $username = '';
 	private $password = '';
 	private $db_name = '';
 	private $db = '';

 	/**
 	*Initialize all database variables
 	*/
 	public function __construct()
 	{
 		$this->host = HOST;
	 	$this->username = USERNAME;
	 	$this->password = PASSWORD;
	 	$this->db_name = DB_NAME;
 	}
 	
 	/**
 	*Connect to Sakila Database
 	*/
	public function Connect()
 	{
		$this->db = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
 	}

	/**
 	*Close the database connection
 	*/
 	public function Close()
 	{
 		mysqli_close($this->db);
 	}

 	/**
 	*Retrieve all actors from database 
 	*/
	function index()
	{
		// Check connection
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
		} 

		$actor = "SELECT * FROM actor;";
		$result = mysqli_query($this->db, $actor);

		if ($result->num_rows > 0) {
			$actors = [];
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		    	array_push($actors, $row);
		    }
		    $json = json_encode($actors, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
			print_r($json);
		} else {
			$error->message = '0 records founded';
			$message = json_encode($error);
			print($message);
		}
		$this->Close();
	}

	/**
	*Create a new actor in the database
	*/
	function create()
	{
		// Check connection
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
		} else {
  			if (isset($_POST['first_name']) && isset($_POST['last_name'])) {
  				$first_name = $_POST['first_name'];
				$last_name =  $_POST['last_name'];

				$newActor =
					"INSERT INTO actor(first_name, last_name) 
					VALUES('" . $first_name . "', '" . $last_name . "');";

	  			try {
	  				$result = mysqli_query($this->db, $newActor);
	  				if ($result) {
	  					$error->status = "200";
						$error->message = 'New actor added';
						$message = json_encode($error);
						print($message);
		  				$this->Close();
	  				}
	  			} catch (MySQLDuplicateKeyException $e) {
	  				$error->message = $e->getMessage();;
					$message = json_encode($error);
					print($message);
	  				
	  			}
  			} else {
  				$error->message = 'Incorrect parameters';
				$message = json_encode($error);
				print($message);
  			} 			
	 	}
	}

	/**
	*Update actor in the database
	*/
	function update()
	{
		$request = new Request;
		$url = $request->getUrl();
		$url = $request->splitUrl($url);
		$requestURL = explode("/", $url[1]);

		// Check connection
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
		} else {
			parse_str(file_get_contents("php://input"),$_PATCH);
			
			$first_name = '';
			$last_name = '';
			$updatedAttr = [];

			if (isset($requestURL[2])) {
				if (isset($_PATCH['first_name'])) {
					array_push($updatedAttr, 'first_name');
				}
				if (isset($_PATCH['last_name'])) {
					array_push($updatedAttr, 'last_name');
				}
				print_r($updatedAttr);
				$updateActor = "UPDATE actor SET ";
				foreach ($updatedAttr as $key => $value) {
					if ($key < sizeof($updatedAttr)-1) {
						if (is_numeric($_PATCH[$value])) {
							$updateActor .= $value . "= " . $_PATCH[$value] . ", ";
						} else {
							$updateActor .= $value . "= '" . $_PATCH[$value] . "', ";
						}
					} else {
						if (is_numeric($_PATCH[$value])) {
							$updateActor .= $value . "= " . $_PATCH[$value]. " WHERE actor_id=$requestURL[2]";
						} else {
							$updateActor .= $value . "= '" . $_PATCH[$value]. "' WHERE actor_id=$requestURL[2]";
						}
					}
				}
				try {
					if ($updateActor === "UPDATE actor SET ") {
						$error->message = 'you must specify attributtes';
						$message = json_encode($error);
						print($message);
						return;
					} else {
						$result = mysqli_query($this->db, $updateActor);
						if ($result) {
							$error->status = "200";
							$error->message = 'Record updated successfully';
							$message = json_encode($error);
							print($message);
						}
					}
				} catch (Exception $e) {
					$error->message = 'Error adding new customer';
					$message = json_encode($error);
					print($message);	
				}
			} else {
					$error->message = 'You must specify the customer';
					$message = json_encode($error);
					print($message);
			}	
		}
	}
}