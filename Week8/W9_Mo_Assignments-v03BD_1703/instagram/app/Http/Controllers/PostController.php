<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Validator;

class PostController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('post');
    }

    /**
     * @param Request $request
     *
     * Validates the request
     * Pass values to Post Model to insert a new post
     *
     * @return $this
     */
    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'picture' => 'required|mimes:jpg,jpeg,png',
        ]);

        if ($validator->fails()) {
            $error = 'The type of image must be jpg, jpeg, or png';

            return view('post')->with(['error' => $error]);
        }

        try {
            $path = $request->file('picture')->store('public/images');
            $mainUser = auth()->user()->id;
            $file = basename($path);

            if ($request->input('caption') !== null) {
                Post::create(['postImage' => 'storage/images/' . $file,
                    'caption' => $request->input('caption'),
                    'userID' => $mainUser]);

                $success = 'Image Inserted successfully';

                return view('post')->with(['success' => $success]);
            } else {
                Post::create(['postImage' => 'storage/images/' . $file,
                    'caption' => '',
                    'userID' => $mainUser]);

                $success = 'Image Inserted successfully';

                return view('post')->with(['success' => $success]);
            }
        } catch (\Exception $e) {
            $error = 'Image not added, something went wrong';

            return view('post')->with(['error' => $error]);
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function addComment(Request $request)
    {
        $user = auth()->user()->id;
        $credentials = $request->only([
            'comment', 'postID'
        ]);

        $validator = Validator::make($credentials, [
            'comment' => 'required',
            'postID' => 'required',
        ]);
        if ($validator->fails()) {
            $error = 'Failed to add comment';
            $user = auth()->user()->id;
            $postsInfo = Post::where('userID', '!=', $user)->orderBy('postID', 'DESC')->get();
            $userPost = [];
            $postComments = [];

            foreach ($postsInfo as $post) {
                $postUser = User::find($post->userID);
                $comments = Comment::where('postID', $post->postID)->get();
                array_push($postComments, $comments);
                $currentPost = [
                    'userName' => $postUser->name,
                    'postImage' => $post->postImage,
                    'caption' => $post->caption,
                    'userID' => $post->userID,
                    'postID' => $post->postID,
                    'postUserImage' => $postUser->profilePicture,
                    'comments' => $postComments,
                ];
                array_push($userPost, $currentPost);
            }
            return view('home')->with(['posts' => $userPost, 'error' => $error]);
        }

        try {
            $data = new Comment([
                'commentText' => $request->comment,
                'userID' => $user,
                'postID' => $request->postID
            ]);
            $data->save();

            return response()->json(['comment' => 'success']);
        } catch (\Exception $e) {
            $error = 'Failed to add comment';
            $user = auth()->user()->id;
            $postsInfo = Post::where('userID', '!=', $user)->orderBy('postID', 'DESC')->get();
            $userPost = [];
            $postComments = [];

            foreach ($postsInfo as $post) {
                $postUser = User::find($post->userID);
                $comments = Comment::where('postID', $post->postID)->get();
                array_push($postComments, $comments);
                $currentPost = [
                    'userName' => $postUser->name,
                    'postImage' => $post->postImage,
                    'caption' => $post->caption,
                    'userID' => $post->userID,
                    'postID' => $post->postID,
                    'postUserImage' => $postUser->profilePicture,
                    'comments' => $postComments,
                ];
                array_push($userPost, $currentPost);
            }
            return view('home')->with(['posts' => $userPost, 'error' => $error]);
        }
    }
}
