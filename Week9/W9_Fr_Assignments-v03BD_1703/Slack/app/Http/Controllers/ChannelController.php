<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Member;
use App\Message;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Validator;

class ChannelController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getChannel()
    {
        return view('channel');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newChannel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'channelName' => 'required',
            'channelPurpose' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->HomeWithError($validator->messages());
        }

        $user = auth()->user()->id;
        Channel::create(['name' => $request->input('channelName'),
            'purpose' => $request->input('channelPurpose'),
            'admin_id' => $user,
        ]);

        $newChannel = Channel::orderBy('id', 'desc')->first();

        Member::create(['user_id' => $user,
            'channel_id' => $newChannel->id,
        ]);

        return $this->Home();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newMember(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->HomeWithError($validator->messages());
        }
        try {
            $email = User::where('email', $request->email)->get();
            if ($email->count() === 0) {
                $error = 'Email not found';
                return $this->HomeWithError($error);
            } else {
                Member::create(['user_id' => $email[0]->id,
                    'channel_id' => $request->id,
                ]);

                return $this->redirectHome();
            }
        } catch (\Exception $e) {
            $error = 'Error adding a member right now. Please try again later';
            return $this->HomeWithError($error);
        }

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function Home()
    {
        $userMembership = [];
        $user = auth()->user();
        $members = $user->member()->where('user_id', $user->id)->pluck('channel_id');
        $conversations = Message::where('channel_id', 1)->get();
        $channelAdmin = Channel::where('admin_id', $user->id)->pluck('id');

        foreach ($members as $member) {
            $channels = Channel::where('id', $member)->select('id', 'name')->get();
            array_push($userMembership, $channels);
        }

        return Redirect::to('home')->with(['channels' => $userMembership, 'conversations' => $conversations, 'channelAdmin' => $channelAdmin]);
    }

    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    public function HomeWithError($error)
    {
        $userMembership = [];
        $user = auth()->user();
        $members = $user->member()->where('user_id', $user->id)->pluck('channel_id');
        $conversations = Message::where('channel_id', 1)->get();
        $channelAdmin = Channel::where('admin_id', $user->id)->pluck('id');

        foreach ($members as $member) {
            $channels = Channel::where('id', $member)->select('id', 'name')->get();
            array_push($userMembership, $channels);
        }

        return Redirect::to('home')->with(['channels' => $userMembership, 'conversations' => $conversations, 'channelAdmin' => $channelAdmin, 'error' => $error]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectHome()
    {
        $userMembership = [];
        $user = auth()->user();
        $members = $user->member()->where('user_id', $user->id)->pluck('channel_id');
        $conversations = Message::where('channel_id', 1)->get();
        $channelAdmin = Channel::where('admin_id', $user->id)->pluck('id');

        foreach ($members as $member) {
            $channels = Channel::where('id', $member)->select('id', 'name')->get();
            array_push($userMembership, $channels);
        }

        return Redirect::to('home')->with(['channels' => $userMembership, 'conversations' => $conversations, 'channelAdmin' => $channelAdmin]);
    }
}