<?php
/*
* each table is consisted of rows
* we can add, get,delete row
*/
Class Table
{
	private $tableName;
	private $rows;
	private $columns;

	public function __construct($args)
	{
		$this->tableName = $args[2];
		$this->columns = array();
		$this->rows = array();
		$this->init($args);
	}
	// inserting columns to the table
	private function init($args)
	{
		for($i = 4; $i < sizeof($args); $i++)
		{
			array_push($this->columns, $args[$i]);
		}
	}
	//returns row data
	public function getRow($index)
	{
		for ($i=0; $i < sizeof($this->rows); $i++) 
		{ 
			$currentRow = $this->rows[$i];
			$result = in_array($index, $currentRow);
			if($result)
			{
				return $currentRow;
			}	
		}
		return null;
	}

	private function getRowIndex($index)
	{
		for ($i=0; $i < sizeof($this->rows); $i++) 
		{ 
			$currentRow = $this->rows[$i];
			$result = in_array($index, $currentRow);
			if($result)
			{
				return $i;
			}	
		}
		return null;
	}
	//delete a specific row based on the index
	public function deleteRow($rowIndex)
	{
		$index = $this->getRowIndex($rowIndex);
		unset($this->rows[$index]);
	}
	//add a new record using add,[info goes here]
	public function addRow($args)
	{
		if (sizeof($this->columns) != sizeof($args)-1)
		{
			echo "Wrong entry\n";
			return false;
		} 
		else 
		{
			$currentRow = array();
			for($i = 0; $i < sizeof($this->columns); $i++)
			{
				//check if first column is number
				if ($i == 0 && !is_numeric($args[$i+1]))
				{
					echo "First column must be an integer\n";
					return false;	
				} else {
					array_push($currentRow, $args[$i+1]);
				}
			}
			array_push($this->rows, $currentRow);
			return true;
		}
		return false;
	}
	public function getTableName()
	{
		return $this->tableName;
	}
}
?>