use sakila;
SELECT first_name, COUNT(FA.actor_id)
FROM actor AS Act, film_actor AS FA
WHERE Act.actor_id = FA.actor_id
GROUP BY FA.actor_id;
