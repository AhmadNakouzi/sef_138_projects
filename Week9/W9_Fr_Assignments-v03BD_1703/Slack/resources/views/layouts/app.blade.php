<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{  'Slack' }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/slackMessenger.js') }}" defer></script>
    @auth()
        <script>var username = "{{auth()->user()->name }}";</script>
@endauth
<!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slackStyle.css') }}" rel="stylesheet">

</head>
<body onload="init()">
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/home') }}">
                {{ 'Slack' }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    @auth()
        {{--SIDEBAR STARTS--}}
        <div class="wrapper">
            <nav id="sidebar" class="w-25 bg-secondary float-left">
                <!-- Sidebar Header -->
                <div class="sidebar-header p-2 w-100 mb-3">
                    <h3 class="float-left">Channels</h3>
                    <a href="{{route('channel') or ''}}"
                       class="btn btn-danger float-right text-white"
                       data-toggle="modal"
                       data-target="#channel">+</a>
                </div>
                @if(isset($channels))

                    @foreach($channels as $channel)
                        <div class="float-left w-100 pl-3">
                            <a id="channels"
                               class="text-white"
                               href="/home/{{$channel[0]['name'] or ''}}">
                                <h4>{{$channel[0]['name'] or ''}}</h4>
                            </a>
                        </div>
                    @endforeach
                @endif
            </nav>
        </div>
        {{--SIDEBAR ENDS--}}
    @endauth

    <main class="py-4">
        @yield('content')
    </main>
    {{--MODAL STARTS--}}
    @auth()
        <div class="modal fade" id="channel" tabindex="-1" role="dialog"
             aria-labelledby="channel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header float-left">
                        <h4 class="modal-title" id="myModalLabel">
                            Create New Channel
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body text-center">
                        <form enctype="multipart/form-data"
                              action="{{route('newChannel')}}"
                              method="post">
                            @csrf
                            <input id="channelName" name="channelName" type="text" class="form-control p-2 mb-3"
                                   placeholder="Channel Name" required>
                            <input id="channelPurpose" name="channelPurpose" type="text" class="form-control p-2 mb-3"
                                   placeholder="Channel Purpose" required>
                            <input type="submit" value="Create" class="btn btn-secondary">
                            <input type="reset" id="resetChannel" value="Reset" class="btn btn-danger">
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endauth
{{--MODAL ENDS--}}

</body>
</html>
