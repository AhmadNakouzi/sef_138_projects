use sakila;
DELIMITER //
CREATE PROCEDURE GetRangeOrMaxC()
BEGIN 
IF EXISTS
(
SELECT name, COUNT(fc.film_id) AS Number
FROM category AS cat, film_category fc, film f
WHERE cat.category_id = fc.category_id
AND fc.film_id = f.film_id
GROUP BY name
HAVING COUNT(fc.film_id) BETWEEN 55 AND  65
)
THEN
SELECT name, COUNT(fc.film_id) AS Number
FROM category AS cat, film_category fc, film f
WHERE cat.category_id = fc.category_id
AND fc.film_id = f.film_id
GROUP BY name
HAVING COUNT(fc.film_id) BETWEEN 55 AND  65;
ELSE 
SELECT name, COUNT(fc.film_id) AS Number
FROM category AS cat, film_category fc, film f
WHERE cat.category_id = fc.category_id
AND fc.film_id = f.film_id
GROUP BY name
ORDER BY Number DESC
LIMIT 3;
END IF;
END //
CALL GetRangeOrMaxC
//
/*
Another way to solve
use sakila;
SELECT
   C.name AS 'Category name', COUNT(C.category_id) AS c
FROM
   category AS C
       JOIN
   film_category AS FC ON FC.category_id = C.category_id
       JOIN
   film AS F ON F.film_id = FC.film_id
GROUP BY C.name
HAVING IF(COUNT(c > 54 AND c < 66) <> 0,
   c > 54 AND c < 66,
   c)
ORDER BY c DESC
*/
