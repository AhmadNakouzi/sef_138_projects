<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Get the user that is being logged in.
     * Get all the blogs that belongs to this user
     * @return \Illuminate\Http\Response with number of blogs
     */
    public function CountUserBlogs()
    {
        $mainUser = Auth::user()->id;
        $userBlogs = Blog::with('userID', $mainUser)->count();
        return view('home', compact('userBlogs'));
    }
}
