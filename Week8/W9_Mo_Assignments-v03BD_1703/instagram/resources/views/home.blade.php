@extends('layouts.app')

@section('content')
    @if(isset($posts) && $posts !== null)
        @foreach($posts as $post)
            <div class="card w-50 mt-3 mb-3 mr-auto ml-auto"
                 @if(isset($post['postID']))
                 id="{{$post['postID']}}"
                    @endif>
                @csrf
                <div class="card-header bg-white">
                    <img src="{{$post['postUserImage'] or 'storage/images/defualt-logo.jpg'}}"
                         width="40px"
                         height="40px"
                         class="cursor rounded-circle">
                    {{$post['userName']}}
                </div>
                <div class="card-body m-0 p-0">
                    <img src="{{$post['postImage'] or '#'}}"
                         width="600px"
                         height="400px"
                         class="cursor w-100"
                         data-toggle="modal"
                         data-target="#imagePreview{{$post['postID']}}">
                </div>

                <div id="controllers" class="w-100">
                    <span class="fa fa-heart-o mr-3 ml-2"></span>
                    <span id="newComment" class="fa fa-comment-o "></span>
                </div>

                <div class="ml-2">
                    {{$post['userName'] . ': ' .$post['caption']}}
                </div>

                <div id="postComments" class="ml-2">
                    @if($post['comments'] !== null)
                        @foreach($post['comments'] as $key => $comment)
                            <span>{{$comment[$key]['userID']  or ''}}</span>
                            <span>{{$comment[$key]['commentText'] or ''}}</span>
                        @endforeach
                    @endif
                </div>

                    <div class=" row w-100 m-auto">
                    <textarea class="form-control p-2 noresize w-100 m-0 border border-dark"
                              name="comment"
                              rows="2"
                              autocomplete="off"
                              placeholder="Comment"
                              onkeydown="if (event.keyCode === 13) addComment();">
                    </textarea>
                    <div>{{$error or ''}}</div>
                </div>
            </div>
            {{--MODAL STARTS--}}
            <div class="modal fade" id="imagePreview{{$post['postID']}}" tabindex="-1" role="dialog"
                 aria-labelledby="imagePreview{{$post['postImage']}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header float-left">
                            <h4 class="modal-title" id="myModalLabel">
                                {{$post['userName']}}
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="{{$post['postImage'] or '#'}}"
                                 width="400px"
                                 height="300px"
                                 class="border border-dark">
                        </div>
                    </div>
                </div>
                {{--MODAL ENDS--}}
            </div>
            </div>
        @endforeach
    @else
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card text-center border-dark">
                    <p>No Posts Yet</p>
                </div>
            </div>
        </div>
    @endif
@endsection