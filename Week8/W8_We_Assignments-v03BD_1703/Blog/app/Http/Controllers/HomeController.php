<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ownBlogs = Blog::where('userID', '=', auth()->user()->id)->count();
        $nbBlogs = Blog::where('userID', '!=', auth()->user()->id)->count();
        return view('home', compact('ownBlogs'), compact('nbBlogs'));
    }
}
