#!/usr/bin/php
<?php
require_once('GameGenerator.php');
require_once('GameSolver.php');
require_once('GameOutput.php');
require_once('Posibilities.php');

echo "How many games would you like me to play today?\n";
$nbOfGames = readline();
while (($nbOfGames == null && $nbOfGames == '' )  || !is_numeric($nbOfGames))
{
	echo "Wrong entry. Please enter a number\n";
	$nbOfGames = readline();
}
$play = new GameGenerator();
$play->PlayGame($nbOfGames);
?>