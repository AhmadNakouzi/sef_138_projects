<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class ProfileController extends Controller
{
    /**
     * Count the posts of the user
     * Find the name of the user
     * Find the profile picture of the user
     *
     * @return $this
     */
    public function index()
    {
        $user = auth()->user()->id;
        $countPosts = Post::where('userID', auth()->user()->id)->count();
        $name = User::find($user)->name;
        $profilePicture = User::find($user)->profilePicture;
        $userPosts = Post::where('userID', $user)->get();

        return view('profile')
            ->with(['countPosts' => $countPosts,
                'name' => $name,
                'picture' => $profilePicture,
                'userPosts' => $userPosts,
            ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProfilePicture()
    {
        return view('editProfilePicture');
    }

    /**
     * @param Request $request
     *
     * Validates the file type
     * Add the image to the storage file
     * Update the user profile image
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function editProfilePicture(Request $request)
    {
        $user = auth()->user()->id;

        $validator = Validator::make($request->all(), [
            'picture' => 'required|mimes:jpg,jpeg,png',
        ]);

        if ($validator->fails()) {
            $error = 'The type of image must be jpg, jpeg, or png';

            return view('profile')->with(['error' => $error]);
        }
        try {
            $oldPicture = User::find($user)->profilePicture;
            $oldPath = basename($oldPicture);
            $path = $request->file('picture')->store('public/images');
            $file = basename($path);

            DB::table('users')
                ->where('id', $user)
                ->update(['profilePicture' => 'storage/images/' . $file]);

            Storage::delete('/public/images/' . $oldPath);


            $countPosts = Post::where('userID', auth()->user()->id)->count();
            $name = User::find($user)->name;
            $profilePicture = User::find($user)->profilePicture;
            $userPosts = Post::where('userID', $user)->get();

            return view('profile')
                ->with(['countPosts' => $countPosts,
                    'name' => $name,
                    'picture' => $profilePicture,
                    'userPosts' => $userPosts,
                ]);
        } catch (\Exception $e) {
            $error = 'Image not added, something went wrong';

            return view('profile')->with(['error' => $error]);
        }
    }

    /**
     * Display editProfile blade
     * @return $this
     */
    public function showProfile()
    {
        $user = auth()->user()->id;
        $name = auth()->user()->name;
        $biography = User::find($user)->biography;
        $phoneNumber = User::find($user)->phoneNumber;

        return view('editProfile')->with(['name' => $name, 'phoneNumber' => $phoneNumber, 'biography' => $biography]);
    }

    /**
     * Edit profile
     * @param Request $request
     * @return $this
     */
    public function editProfile(Request $request)
    {
        $user = auth()->user()->id;
        $validator = Validator::make($request->all(), [
            'phoneNumber' => 'required',
            'biography' => 'required',
            'password' => 'required',
            'newPassword' => 'required',
            'confirmPassword' => 'required',
        ]);

        if ($validator->fails()) {
            $error = 'Something went Wrong';

            $user = auth()->user()->id;
            $name = auth()->user()->name;
            $biography = User::find($user)->biography;
            $phoneNumber = User::find($user)->phoneNumber;

            return view('editProfile')->with(['error' => $error, 'name' => $name, 'phoneNumber' => $phoneNumber, 'biography' => $biography]);
        }

        try {
            $dbPassword = User::find($user)->password;
            $password = $request->input('password');
            $newPassword = $request->input('newPassword');
            $confirmPassword = $request->input('confirmPassword');
            $currentPassword = password_verify($password, $dbPassword);

            if ($currentPassword && $newPassword === $confirmPassword && strlen($newPassword) >= 6) {
                DB::table('users')
                    ->where('id', $user)
                    ->update(['biography' => $request->input('biography'),
                        'name' => $request->input('name'),
                        'phoneNumber' => $request->input('phoneNumber'),
                        'password' => bcrypt($newPassword)]);

                $countPosts = Post::where('userID', auth()->user()->id)->count();
                $name = User::find($user)->name;
                $profilePicture = User::find($user)->profilePicture;
                $userPosts = Post::where('userID', $user)->get();
                $phoneNumber = User::find($user)->phoneNumber;

                return view('profile')
                    ->with(['countPosts' => $countPosts,
                        'name' => $name,
                        'picture' => $profilePicture,
                        'userPosts' => $userPosts,
                        'phoneNumber' => $phoneNumber,
                    ]);
            } else {
                $error = 'Incorrect Password';
                $user = auth()->user()->id;
                $name = auth()->user()->name;
                $biography = User::find($user)->biography;
                $phoneNumber = User::find($user)->phoneNumber;

                return view('editProfile')->with(['error' => $error, 'name' => $name, 'phoneNumber' => $phoneNumber, 'biography' => $biography]);
            }
        } catch (\Exception $e) {
            $error = 'Profile can not be edited now, something went wrong';
            $user = auth()->user()->id;
            $name = auth()->user()->name;
            $biography = User::find($user)->biography;
            $phoneNumber = User::find($user)->phoneNumber;

            return view('editProfile')->with(['error' => $error, 'name' => $name, 'phoneNumber' => $phoneNumber, 'biography' => $biography]);
        }
    }
}