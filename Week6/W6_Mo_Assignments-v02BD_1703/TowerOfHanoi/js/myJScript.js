'use strict';
var myTimer = null, directionsStack = [];

//the destination stack will contain the initial and destination bar for a specific bar

function executeHanoi() {
    Hanoi(8, 0, 2, 1);
    myTimer = setInterval(movingDiskAnimation, 200);
}

// tower of hanoi algorithm
function Hanoi(n, from, to, via) {
    if (n === 0)
        return;
    Hanoi(n - 1, from, via, to);
    directionsStack.push([from, to]); // save parameters to callStack array
    Hanoi(n - 1, via, to, from);
}

function movingDiskAnimation() {

    var bar1 = document.getElementById('barDiv1');
    var bar2 = document.getElementById('barDiv2');
    var bar3 = document.getElementById('barDiv3');

    if (directionsStack.length === 0) {
        return;
    }
    else {
        var movement, initBar, destBar;

        for (var i = 0; i < directionsStack.length; i++) {
            movement = directionsStack.pop();
            initBar = movement[0];
            destBar = movement[1];

            if (initBar === 0) {
                if (destBar === 2) {
                    bar2.appendChild(bar1.firstElementChild);
                }
                else {
                    bar3.appendChild(bar1.firstElementChild);
                }
            }
            else if (initBar === 1) {
                if (destBar === 1) {
                    bar1.appendChild(bar2.firstElementChild);
                }
                else {
                    bar3.appendChild(bar2.firstElementChild);
                }
            }
            else {
                if (destBar === 1) {
                    bar1.appendChild(bar3.firstElementChild);
                }
                else {
                    bar2.appendChild(bar3.firstElementChild);
                }
            }
        }
    }
}