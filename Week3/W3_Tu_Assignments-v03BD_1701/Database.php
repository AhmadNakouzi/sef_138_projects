<?php
	Class Database
	{
		private $databaseName;
		private $tablesList;

		public function __construct($databaseName) 
		{
			$this->databaseName = $databaseName;
			$this->tablesList = array();
		}
		public function getDatabaseName() 
		{
			return $this->databaseName;
		}
		public function getTables() 
		{
			return $this->tablesList;
		}
		public function addTable($table)
		{
			if (in_array($table, $this->tablesList)) 
			{
				return 0;
			}
			array_push($this->tablesList, $table);
			return 1;
		}
	}
?>