<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIDs = DB::table('users')->select('id')->first();
        $channelIDs = DB::table('channels')->select('id')->first();

        DB::table('messages')->insert([
            'message' => str_random(50),
            'user_id' => rand(1, $usersIDs->id),
            'channel_id' => rand(1, $channelIDs->id),
        ]);
    }
}
