<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class UserCommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIDs = DB::table('users')->select('id')->first();
        $postIDs = DB::table('user_posts')->select('postID')->first();

        DB::table('user_comments')->insert([
            'commentText' => str_random(),
            'userID' => rand(1, $usersIDs->id),
            'postID' => rand(1, $postIDs->postID),
        ]);
    }
}
