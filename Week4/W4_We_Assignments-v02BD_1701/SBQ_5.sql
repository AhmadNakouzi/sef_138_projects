use sakila;
SELECT first_name AS 'FIRST NAME', last_name AS 'LAST NAME', release_year AS 'RELEASE YEAR' 
FROM actor AS A, film_actor AS FA, film AS F
WHERE A.actor_id = FA.actor_id
AND FA.film_id = F.film_id
AND FA.film_id IN
(SELECT film_id FROM film_text WHERE description LIKE '%Crocodile%' OR description LIKE '%Sharp%')
GROUP BY first_name, last_name,release_year
ORDER BY last_name ASC;
