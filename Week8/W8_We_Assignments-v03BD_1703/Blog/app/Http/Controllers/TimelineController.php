<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class TimelineController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * retrieve all blogs that don't belong to the user
     *
     * Show the timeline blade
     *
     * @return \Illuminate\Http\Response
     */
    public function timeLine()
    {
        Auth::user()->id;
        $blogs = Blog::where('userID', '!=', auth()->user()->id)->paginate(3);
        return view('timeline', compact('blogs'));
    }

    /**
     * retrieve all blogs that belong to the user
     *
     * list my blogs in a blade
     *
     * @return \Illuminate\Http\Response
     */
    public function myBlogs()
    {
        $myBlogs = Blog::where('userID', '=', auth()->user()->id)->paginate(3);
        return view('myBlogs', compact('myBlogs'));
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function newBlogBlade()
    {
        return view('newBlogs');
    }

    /**
     * Create a new blog.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function newBlog(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator);
            }

            Blog::create([
                'title' => $request['title'],
                'description' => $request['description'],
                'userID' => auth()->user()->id
            ]);
            return redirect('myBlogs');
        } catch (\Exception $e) {
            return back()
                ->with('error', $e->getMessage())
                ->withInput();
        }
    }

    /**
     * remove a specific blog.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function removeBlog($id)
    {
        try {
            Blog::FindOrFail($id)->delete();
            return view('home');
        } catch (\Exception $e) {
            return back()
                ->with('error', $e->getMessage())
                ->withInput();
        }
    }

    /**
     * read more about a specific blog.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function readBlogMore($id)
    {
        try {
            $blogContent = Blog::FindOrFail($id);
            return view('blogContent', compact('blogContent'));
        } catch (\Exception $e) {
            return back()
                ->with('error', $e->getMessage())
                ->withInput();
        }
    }

    /**
     * TODO
     * edit a specific blog.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function editMyBlog(Request $request, $id)
    {
        $blog = Blog::find($id);

        try {
            if (($request->input('title') != null) and ($request->input('description') != null)) {
                $blog->update(['title' => $request->input('title'), 'description' => $request->input('description')]);
            }
        } catch (\Exception $e) {
            return back()
                ->with('error', $e->getMessage())
                ->withInput();
        }
    }
}
