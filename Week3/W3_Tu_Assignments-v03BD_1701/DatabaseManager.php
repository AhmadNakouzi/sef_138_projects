<?php
require_once "Database.php";
require_once "Table.php";

$databaseList = array();
$currentDatabaseIndex = 0;
$currentTableIndex = 0;
echo "Enter commands to start:\n";
while (true)
{
	$value = readline();
	$args = explode(",",trim($value));
	$arg0 = strtolower($args[0]);
	if($arg0 == 'exit')
	{
		exit;
	}
	switch ($arg0) 
	{
		case 'create':
			if (!isset($args[1]) || trim($args[1]) == '')
			{
				echo "You must specify what you want to create? \n";
				break;	
			}
			$arg1 = strtolower(trim($args[1]));
			switch ($arg1) 
			{
				//case of creating a new database
				case 'database':
					if (!isset($args[2]) || trim($args[2]) == '')
					{
						echo "You must enter the name of database\n";
						break;
					}
					$folder = trim($args[2]);
					if (file_exists($folder))
					{ 
						echo "Database already exists\n";
						break;
					}
					mkdir($folder);
					chmod("$folder", 0766);
					$filename = "$folder/$folder.csv";
					//creating a new database 
					// $json = json_encode($databaseList);
					$database = new Database($folder);
					//push the new database to the list that contain all databases that are already created
					array_push($databaseList, $database);
					//get the index of database from the list to work on it
					$currentDatabaseIndex = SearchDbIndex($folder, $databaseList);
					//open a csv file to save the data of the database created 
					$dbFile = fopen($filename, "a");
					//formatting the data that is inserted in csv file
					$dbOutputName = "Database Name,$folder\n";
	        		//put the data in the created file 
	        		fputs($dbFile,$dbOutputName);
	        		//close the file without freeing up the memory from saved data
	        		fclose($dbFile);
	        		//free up memory from saved data
	        		unset($dbOutputName);
	        		//informing the user that the database has been created successfully
					echo "\"$folder\" CREATED\n";
					//get out to take another command
					break;

				case 'table':
					//validate if the name of table is entered 
					if (!isset($args[2]) || trim($args[2]) == '')
					{
						echo "You must enter the name of table\n";
						break;
					}
					//validate user is not creating empty tables [without columns]
					if (!isset($args[3]))
					{
						echo "You should specify columns\n";
						break;
					}
					$arg4 = strtolower(trim($args[3]));
					//check if the table contains columns 
					if (trim($arg4) == '' || $arg4 != "columns")
					{
						//no columns assigned for table
						echo "You should specify columns\n";
						break;
					} 
					if (!isset($args[4]))
					{
						echo "You must label the columns\n";
						break;
					}
					//all conditions are true, --> create a new table 
					$table = new Table($args);
					//addTable function will return 1 if table created successfully or 0 on failure
					$result = $databaseList[$currentDatabaseIndex]->addTable($table);
					if ($result == 0) 
					{
						echo "Table already exists\n";
						break;
					}
					$currentTableIndex = SearchTableIndex($table->getTableName(),$databaseList[$currentDatabaseIndex]->getTables());

					$currentDatabaseName = end($databaseList)->getDatabaseName();
					// print_r($currentDatabaseName);exit;
					$databaseFolder = "$currentDatabaseName/$currentDatabaseName.csv";
					//open a csv file to save the data of the database created 
					$dbFile = fopen($databaseFolder, "a");
					//formatting the data that is inserted in csv file
					$TableOutputName = "Table Name,$args[2]\n";
	        		//put the created table in the database file 
	        		fputs($dbFile, $TableOutputName);
	        		fputs($dbFile, "columns,");
	        		for ($i = 4; $i < sizeof($args) ; $i++)
	        		{ 
	        			if ($i == sizeof($args)-1)
						{
							//since it is the last element so we don't need comma
							fputs($dbFile,"$args[$i]\n");
							break;
						}
						else
						{
	        				//insert the columns into the database file with comma
	        				fputs($dbFile,"$args[$i],");
	        			}
	        		}
	        		//close the file without freeing up the memory from saved data
	        		fclose($dbFile);
	        		//informing the user that the database has been created successfully
					echo "\"$args[2]\" CREATED\n";
	        		//free up memory from saved data
	        		unset($TableOutputName);
					//get out to take another command
					break;
				default:
					//syntax error on creating
					echo "Wrong entry\n";
					break;
			}
			break;
		//case of adding a new record
		case 'add':
			if (!isset($args[1]) || trim($args[1]) == '')
			{
				echo "You must specify what you want to add? \n";
				break;	
			}
			$currentTableIndex = SearchTableIndex($table->getTableName(),$databaseList[$currentDatabaseIndex]->getTables());
			//get last created database 
			$currentDatabaseName = end($databaseList)->getDatabaseName();
			// print_r($currentDatabaseName);exit;
			$databaseFolder = "$currentDatabaseName/$currentDatabaseName.csv";
			//open a csv file to save the data of the database created 
			$dbFile = fopen($databaseFolder, "a");
			fputs($dbFile, "row,");
			for ($i = 1; $i < sizeof($args) ; $i++)
			{ 
				//adding last element
				if ($i == sizeof($args)-1)
				{
					fputs($dbFile, "$args[$i]\n");
					break;
				}
				else
				{
					//insert the columns into the database file
					fputs($dbFile, "$args[$i],");
				}
			}
			//close the file without freeing up the memory from saved data
			fclose($dbFile);
			//free up memory from saved data
			unset($databaseFolder);
			//send the index of the database currently working on with a specific table
			$returnedResult = $databaseList[$currentDatabaseIndex]->getTables()[$currentTableIndex]->addRow($args);
			if ($returnedResult == true)
			{
				echo "RECORD ADDED\n";	
			}
			break;
		//delete either whole database or specific record
		case 'delete':
			if (!isset($args[1]) || trim($args[1]) == '')
			{
				echo "You must specify what you want to delete? \n";
				break;	
			}
			$arg1 = strtolower($args[1]);
			switch ($arg1) {
					//delete whole database
					case 'database':
						DeleteDatabase($args[2], $databaseList);
						break;
					//delete w specific row from table
					case 'row':
						$databaseList[$currentDatabaseIndex]->getTables()[$currentTableIndex]->deleteRow($args[2]);
						$currentDatabaseName = end($databaseList)->getDatabaseName();
						DeleteRow("$currentDatabaseName/$currentDatabaseName.csv", $args[2]);
						break;
					
					default:
						echo "Wrong entry\n";
						break;
				}
				break;
		//selecting a row from table
		case 'get':
			$currentDatabaseName = end($databaseList)->getDatabaseName();
			$result = SearchRow("$currentDatabaseName/$currentDatabaseName.csv", $args[1]);
			break;

		default:
			//syntax error [wrong command entered]
			echo "Wrong entry\n";
			break;
	}
}
//this function will delete a specific database on call
function DeleteDatabase($databaseName,$databaseList = array())
{
	//go through the list of databases 
	for($i = 0; $i < sizeof($databaseList); $i++)
	{
		//get the database name on each index to compare it 
		$name = $databaseList[$i]->getDatabaseName();
		if($databaseName == $name)
		{
			//since the if condition is true, then we will remove the database from the list
			unset($databaseList[$i]);
		}
	}
	//Search for the folder of database to remove it
	//folder is not empty so it must be scanned using scandir
	if (!file_exists($databaseName))
    {
    	echo "$databaseName database does not exists\n";
        return true;
    }

    if (!is_dir($databaseName))
    {
        return unlink($databaseName);
    }

    foreach (scandir($databaseName) as $item)
    {
        if ($item == '.' || $item == '..')
        {
            continue;
        }
        if (!DeleteDatabase($databaseName . DIRECTORY_SEPARATOR . $item))
        {
        	echo "Database not found\n";
            return false;
        }
    }
    echo "\"$databaseName\" Deleted\n";
    return rmdir($databaseName);
}
//get the index of a specific database from the list of databases
function SearchDbIndex($dbName,$databaseList)
{
	for($i = 0; $i < sizeof($databaseList); $i++)
	{
		$currentDatabase = $databaseList[$i];
		if ($currentDatabase->getDatabaseName() == $dbName)
		{
			return $i;
		}
	}
}
//get the index of a specific table from the list of tables
function SearchTableIndex($tableName,$tables)
{
	for ($i = 0; $i < sizeof($tables); $i++)
	{
		$currentTable = $tables[$i];
		if ($currentTable->getTableName() == $tableName)
		{
			return $i;
		}
	}
}

function SearchRow($filename, $element)
{
	//open file that will read row by row to search if the element is found
	if (($handle = fopen("$filename", "r")) !== FALSE)
	{
	    $result = false;
	    while ($row = fgetcsv($handle))
	    {
	    	foreach ($row as $key => $value)
	    	{
	    		if ($row[$key] == $element)
	    		{
		            $result = $row;
	        	}	
	    	}  
	    }
	    if ($result != false)
	    {
	    	for ($i=1; $i < sizeof($result) ; $i++)
	    	{ 
	    		if ($i == sizeof($result)-1)
	    		{
	    			echo "\"$result[$i]\"\n";
	    		}
	    		else
	    		{
	    			echo "\"$result[$i]\",";
	    		}
	    	}
	    }
	    else 
	    {
	    	echo "$element not found\n";
	    	fclose($handle);
	    }
	}
}

//Search for a given value to be deleted
function DeleteRow($file,$element)
{
	//check if it is number
	if (!is_numeric($element))
	{
		echo "You must enter integer value\n";
		return;
	}
	$i=0;
	$array=array();
	
	$read = fopen($file, "r");
	while(!feof($read))
	{
		$array[$i] = fgets($read);	
		++$i;
	}
	fclose($read);
	
	$write = fopen($file, "w");
	foreach($array as $a)
	{
		if(!strstr($a,$element))
		{
			fwrite($write,$a);
		}
	}
	echo "$element DELETED\n";
	fclose($write);
}
?>