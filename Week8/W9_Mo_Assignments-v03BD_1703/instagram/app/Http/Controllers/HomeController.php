<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get all posts that don't belong to the user to be previewed in the home page
     * @return $this
     */
    public function index()
    {
        $user = auth()->user()->id;
        $postsInfo = Post::where('userID', '!=', $user)->orderBy('postID', 'DESC')->get();
        $userPost = [];
        $postComments = [];

        foreach ($postsInfo as $post) {
            $postUser = User::find($post->userID);
            $comments = Comment::where('postID', $post->postID)->get();
            array_push($postComments, $comments);
            $currentPost = [
                'userName' => $postUser->name,
                'postImage' => $post->postImage,
                'caption' => $post->caption,
                'userID' => $post->userID,
                'postID' => $post->postID,
                'postUserImage' => $postUser->profilePicture,
                'comments' => $postComments,
            ];
            array_push($userPost, $currentPost);
        }
        return view('home')->with(['posts' => $userPost]);
    }
}