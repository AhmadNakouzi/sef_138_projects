use sakila;
SELECT country AS 'TOP 3 COUNTRIES'
FROM customer_list
GROUP BY country
ORDER BY country DESC
LIMIT 3;
