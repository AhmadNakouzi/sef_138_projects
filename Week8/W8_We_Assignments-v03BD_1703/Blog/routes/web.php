<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('timeline', ['as' => 'timeline.show', 'uses' => 'TimelineController@timeLine']);
    Route::get('timeline/{id}', ['as' => 'readMore', 'uses' => 'TimelineController@readBlogMore']);

    Route::get('newBlog', ['as' => 'newBlog.show', 'uses' => 'TimelineController@newBlogBlade']);
    Route::post('newBlog', ['as' => 'newBlog.post', 'uses' => 'TimelineController@newBlog']);

    Route::get('myBlogs', ['as' => 'myBlogs', 'uses' => 'TimelineController@myBlogs']);
    Route::get('myBlogs/{id}', ['as' => 'removeMyBlog', 'uses' => 'TimelineController@removeBlog']);
    Route::post('myBlogs/{id}', ['as' => 'editMyBlog', 'uses' => 'TimelineController@editMyBlog']);
});
