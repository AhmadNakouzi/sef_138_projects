var socket;

/**
 *
 * @param host
 * @returns {*}
 */
function createSocket(host) {

    if ('WebSocket' in window)
        return new WebSocket(host);
    else if ('MozWebSocket' in window)
        return new MozWebSocket(host);

    throw new Error("No web socket support in browser!");
}

/**
 * open web socket to start chatting
 */
function init() {
    var host = "ws://localhost:12345/chat";

    try {
        socket = createSocket(host);

        socket.onmessage = function (msg) {
            log(msg.data);
            logChat();
        };
        socket.onclose = function (msg) {
            log("Disconnected- Goodbye");
        };
        logChat();
    } catch (ex) {
        console.log(ex);
    }
    document.getElementById("msg").focus();
}

/**
 * Close the socket
 */
function quit() {
    log("Goodbye!");
    socket.close();
    socket = null;
}

/**
 * Append the message to the chat div
 * @param msg
 */
function log(msg) {
    document.getElementById("log").innerHTML += "<br>" + msg;
}

function onkey(event) {
    if (event.keyCode === 13) {
        saveChat();
    }
}

/**
 * Saves the message in the database
 */
function saveChat() {
    var message = document.getElementById('msg').value;

    try {
        socket.send(username + ' : ' + message);
        document.getElementById("msg").value = '';

        var data = 'msg=' + message;
        var request = new XMLHttpRequest();
        var token = document.getElementsByTagName('meta')['csrf-token'].content;

        request.open("POST", "saveChat", true);
        request.setRequestHeader("X-CSRF-Token", token);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send(data);
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                console.log()
            }
        }
        logChat();
    } catch (ex) {
        console.log(ex);
    }
}

function logChat() {
    var logChat = document.getElementById("log");
    logChat.scrollTop = logChat.scrollHeight;
}