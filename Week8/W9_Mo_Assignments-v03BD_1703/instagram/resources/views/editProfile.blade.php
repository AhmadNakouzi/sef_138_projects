@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Profile Info</div>
                    <div class="card-body">
                        <div id="errorMessage" class="text-danger">{{$error or ''}}</div>
                        <form enctype="multipart/form-data"
                              action="{{route('editProfile.upload')}}"
                              method="post">
                            @csrf
                            <div class="clear margin-bottom"></div>
                            <div id="profileInfo"
                                 class="col-4">

                                <label for="name">Name</label>
                                <input type="text"
                                       id="name"
                                       name="name"
                                       placeholder="Name"
                                       class="p-1"
                                       value="{{$name or ''}}"
                                       required>

                                <label for="phoneNumber">Phone Number</label>
                                <input type="text"
                                       id="phoneNumber"
                                       name="phoneNumber"
                                       placeholder="Phone Number"
                                       class="p-1"
                                       value="{{$phoneNumber or ''}}"
                                       required>

                                <label for="biography">Biography</label>
                                <input type="text"
                                       id="biography"
                                       name="biography"
                                       placeholder="Biography"
                                       class="p-1"
                                       value="{{$biography or ''}}"
                                       required>

                                <label for="password">Old Password</label>
                                <input type="password"
                                       id="password"
                                       name="password"
                                       placeholder="old password"
                                       class="p-1"
                                       required>

                                <label for="newPassword">New Password</label>
                                <input type="password"
                                       id="newPassword"
                                       name="newPassword"
                                       placeholder="new password"
                                       class="p-1"
                                       required>

                                <label for="confirmPassword">Confirm Password</label>
                                <input type="password"
                                       id="confirmPassword"
                                       name="confirmPassword"
                                       placeholder="confirm password"
                                       class="p-1"
                                       required>

                            </div>
                            <div class="clear"></div>
                            <input class="btn bg-primary mt-2" type="submit" value="Save Changes">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection