var itemName = document.getElementById('itemName');
var addItem = document.getElementById('addItem');
var itemDesc = document.getElementById('itemDescription');
var itemsAdded = document.getElementById('itemsAdded');
var newDivItem, itemDate;

if (localStorage.getItem('counter')) {
    var counter = localStorage.getItem('counter');
}
else {
    counter = 1;
}

window.onload = (function () {
    for (var i = 1; i < localStorage.length; i++) {
        var item = JSON.parse(localStorage.getItem(localStorage.key(i)));
        var id = item.id;
        var name = item.name, date = item.date, desc = item.desc;
        newItem(id, name, date, desc);
    }
});

addItem.addEventListener("click", function add() {
    if (!validateInput()) {
        alert("you must enter required information");
        return false;
    }
    else {
        try {
            localStorage.setItem('counter', JSON.stringify(++counter));
            counter--;
            newItem();
            saveInLocalStorage();
            counter++;
            itemName.value = '';
            itemDesc.value = '';
        }
        catch (e) {
            throw {
                name: 'errorAddingItem',
                message: 'Error in adding new item'
            };
        }
    }
});

function validateInput() {
    if (itemName.value.trim() !== '' && itemDesc.value.trim() !== '') {
        return true;
    }
    return false;
}

function newItem(id, itemTitle, itemCreatedDate, itemDescription) {
    newDivItem = document.createElement('div');
    var item = document.createElement('div');
    var rmItem = document.createElement('div');
    var itemName = document.createElement('h1');
    itemDate = document.createElement('p');
    var itemDesc = document.createElement('h4');
    var rmBtn = document.createElement('button');

    if (itemTitle == null && itemCreatedDate == null && itemDescription == null) {
        //initialize variables
        itemName.innerHTML = this.itemName.value;
        itemDesc.innerHTML = this.itemDesc.value;
        itemDate.innerHTML = "added: " + new Date().toDateString();
        rmBtn.id = 'rmItem' + counter;
        newDivItem.id = 'item' + counter;
    } else {
        itemName.innerHTML = itemTitle;
        itemDesc.innerHTML = itemDescription;
        itemDate.innerHTML = itemCreatedDate;
        newDivItem.id = id;
        rmBtn.id = id;
    }
    rmBtn.innerHTML = 'X';

    //item details css
    itemName.style.margin = '0';
    itemDate.style.margin = '0';
    itemDesc.style.margin = '0';

    //newDivItem css
    var newDivItemStyle = newDivItem.style;
    newDivItemStyle.border = '1px solid #000';
    newDivItemStyle.borderRadius = '5px';
    newDivItemStyle.marginBottom = '10px';
    newDivItemStyle.padding = '10px';
    newDivItemStyle.width = '94%';
    newDivItemStyle.overflow = 'hidden';
    newDivItemStyle.position = 'relative';

    //itemTitle css
    var itemStyle = item.style;
    itemStyle.borderRight = '1px solid #000';
    itemStyle.width = '93%';
    itemStyle.marginRight = '5px';
    itemStyle.cssFloat = 'left';

    //rmItem css
    var rmItemStyle = rmItem.style;
    rmItemStyle.width = '4.9%';
    rmItemStyle.cssFloat = 'right';
    rmItemStyle.marginTop = '7%';

    //rmBtn css
    rmBtn.className = 'removeItem';

    //inserting items detail in item div
    item.appendChild(itemName);
    item.appendChild(itemDate);
    item.appendChild(itemDesc);

    //inserting remove button into remove Div
    rmItem.appendChild(rmBtn);

    newDivItem.appendChild(item);
    newDivItem.appendChild(rmItem);
    itemsAdded.appendChild(newDivItem);
}

function saveInLocalStorage() {
    var itemDetails = {
        "id": this.newDivItem.id,
        "name": this.itemName.value,
        "date": this.itemDate.innerHTML,
        "desc": this.itemDesc.value
    };
    localStorage.setItem('item' + counter, JSON.stringify(itemDetails));
}

//add event listener onClick to remove item from todoList
document.addEventListener('click', function (e) {
    e = e || window.event;

    if (e.target.className === 'removeItem') {
        var element = e.srcElement;
        var parentElement = document.getElementById(element.parentElement.parentElement.id);
        element.setAttribute('click', removeElement(parentElement));
    }
});

//remove item from list
function removeElement(childDiv) {
    var child = document.getElementById(childDiv.id);
    var parent = document.getElementById('itemsAdded');

    parent.removeChild(child);
    localStorage.removeItem(childDiv.id);
    if(localStorage.length === 1)
    {
        localStorage.clear();
        counter = 1;
    }
}