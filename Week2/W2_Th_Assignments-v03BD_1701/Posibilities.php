<?php 
class Posibilites
{
    function Permutation($choosenRandNumbers, $InProcessedArray = array())
    {
        $ReturnArray = array();//To save the final possible results 
        foreach($choosenRandNumbers as $Key => $value)
        {
            //Switching between the elements of the array
            $CopyArray = $InProcessedArray;
            $CopyArray[$Key] = $value;
            $TempArray = array_diff_key($choosenRandNumbers, $CopyArray);//If the array is already taken before, it will skip it using if-else condition  
            if (count($TempArray) == 0) {
                $ReturnArray[] = $CopyArray;// Get a new possible way
            } else {
                $ReturnArray = array_merge($ReturnArray, $this->Permutation($TempArray, $CopyArray));//Adding the possible way to the results
            }
        }
        return $ReturnArray;
    }
}
?>