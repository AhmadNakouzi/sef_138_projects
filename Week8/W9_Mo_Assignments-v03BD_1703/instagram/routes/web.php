<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index');
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/profile','ProfileController@index')->name('profile');
    Route::get('/post/show', 'PostController@show')->name('post.show');
    Route::post('/post/upload', 'PostController@upload')->name('post.upload');
    Route::get('/editProfilePicture', 'ProfileController@showProfilePicture')->name('editProfilePicture.show');
    Route::post('/editProfilePicture', 'ProfileController@editProfilePicture')->name('editProfilePicture.upload');
    Route::get('/editProfile', 'ProfileController@showProfile')->name('profile.show');
    Route::post('/editProfile', 'ProfileController@editProfile')->name('editProfile.upload');
    Route::post ( '/addComment', 'PostController@addComment' )->name('addComment');
});