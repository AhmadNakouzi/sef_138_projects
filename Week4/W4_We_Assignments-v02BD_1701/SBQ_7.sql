use sakila;
(SELECT first_name AS 'First Name', last_name AS 'Last Name'
FROM actor
WHERE actor_id != 8
AND first_name = (SELECT first_name FROM actor WHERE actor_id = 8))
UNION 
(SELECT first_name AS 'First Name', last_name AS 'Last Name'
FROM customer 
WHERE first_name = (SELECT first_name FROM actor WHERE actor_id = 8))
