<?php

require_once("config.php");
// require_once("sakilaManager.php");
/**
* 
*/
class AddressesController
{
	private $host = '';
 	private $username = '';
 	private $password = '';
 	private $db_name = '';
 	private $db = '';

 	/**
 	*Initialize all database variables
 	*/
 	public function __construct()
 	{
 		$this->host = HOST;
	 	$this->username = USERNAME;
	 	$this->password = PASSWORD;
	 	$this->db_name = DB_NAME;
 	}
 	
 	/**
 	*Connect to Sakila Database
 	*/
	public function Connect()
 	{
		$this->db = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
 	}

	/**
 	*Close the database connection
 	*/
 	public function Close()
 	{
 		mysqli_close($this->db);
 	}

 	/**
 	*Retrieve all addresses from database 
 	*/
	function index()
	{
		// Check connection
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
		} 

		$address = "SELECT address_id, address, address2, district, city_id, postal_code, phone FROM address;";
		$result = mysqli_query($this->db, $address);

		if ($result->num_rows > 0) {
			$addresses = [];
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		    	array_push($addresses, $row);
		    }

		    $json = json_encode($addresses, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
			print_r($json);
		} else {
		    $error->message = '0 records founded';
			$message = json_encode($error);
			print($message);
		}
		$this->Close();
	}

	/**
	*Create a new address in the database
	*/
	function create()
	{
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
		} else {
			if (isset($_POST['address']) && isset($_POST['address2'])
  				&& isset($_POST['district']) && isset($_POST['city'])
  				&& isset($_POST['phone'])) {
				 // && isset($_POST['location'])
				
				$address = $_POST['address'];
				$address_two = $_POST['address2'];
				$district = $_POST['district'];
				$phone = $_POST['phone'];
				$postal_code = '';
				// $location = $_POST['location'];

				if (isset($_POST['postal_code'])) {
					$postal_code = $_POST['postal_code'];
				}

				$city = $_POST['city'];
				$city_id = $this->validateCity($city);
				$newAddress = "INSERT INTO address(address, address2, district, city_id, postal_code, phone) VALUES('" . $address ."', '" . $address2 . "', '" . $district . "', '". $district ."', " . $city_id . ", " . $postal_code . ", '" . $phone . "');";
				
				try {
	  				$result = mysqli_query($this->db, $newAddress);
	  				if($result)
	  					print('New address added');
	  				else
	  					print('Error inserting new address');
	  			} catch (MySQLDuplicateKeyException $e) {
	  				$e->getMessage();
	  			} finally {
	  				$this->Close();
	  			}

			} else {
  				print "Incorrect parameters";
  			} 
		}
	}

	/**
	*Validates if city exists in database
	*/
	function validateCity($city)
	{
		$city = "SELECT city_id from city WHERE city = '" . $city . "'";
		$city = mysqli_query($this->db, $city);
		$city = mysqli_fetch_assoc($city);
		
		return $city['city_id'];
	}
}