@extends('layouts.app')
@section('content')
    {{--CHANNELS START--}}
    <div class="container w-75 float-right">
        <div class="row ">
            <div class="col-md-12">
                <div id="errorMessage">{{$error or ''}}</div>

                @if(isset($channels))
                    <div class="card" id="{{$channels[0][0]->id or ''}}">
                        <div class="card-header">
                            @if(isset($choosenChannel))
                                <h3>{{$choosenChannel or ''}}</h3>
                            @else
                                <h3>{{$channels[0][0]->name or ''}}</h3>
                            @endif
                        </div>

                        <div class="card-body">
                            <div id="log" class="p-1 w-100 rounded">
                                @if(isset($conversations))
                                    @csrf
                                    @foreach($conversations as $conversation)
                                        <p class="messages mb-1 rounded p-2 bg-primary text-white">
                                            {{$conversation->user->name or ''}} : {{$conversation->message or ''}}
                                        </p>
                                    @endforeach
                                @endif
                            </div>
                            <input id="msg" name="msg" class="form-control mt-3 mb-2 w-100" type="text"
                                   onkeypress="onkey(event)"/>
                            <div class="float-right">
                                @csrf
                                <button onclick="saveChat()" class="btn btn-primary">Send</button>
                                <button onclick="quit()" class="btn btn-danger">Leave</button>
                                @if(isset($channelAdmin))
                                    @foreach($channelAdmin as $isAdmin)
                                        @if($isAdmin === $channels[0][0]->id)
                                            <a href="addMember/{{$channels[0][0]->id}}"
                                               class="btn btn-secondary"
                                               data-toggle="modal"
                                               data-target="#member">Add Member
                                            </a>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    {{--CHANNELS END--}}

    {{--MODAL STARTS--}}
    @auth()
        <div class="modal fade" id="member" tabindex="-1" role="dialog"
             aria-labelledby="member">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header float-left">
                        <h4 class="modal-title" id="myModalLabel">
                            Add New Member
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body text-center">
                        <form enctype="multipart/form-data"
                              action="addMember/{{$channels[0][0]->id or ''}}"
                              method="post">
                            @csrf
                            <input id="email" name="email" type="email" class="form-control p-2 mb-3"
                                   placeholder="Member Email" required>
                            <input type="submit" value="Add" class="btn btn-secondary w-25">
                            <input type="reset" value="Reset" class="btn btn-danger w-25">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endauth
    {{--MODAL ENDS--}}
@endsection
