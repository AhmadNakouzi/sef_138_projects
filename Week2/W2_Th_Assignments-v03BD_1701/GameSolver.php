<?php
class GameSolver
{
	public function GameSolver($counter, $choosenRandNumbers, $possibileNumbers, $possibileOperators, $target)
	{
		$result = $this->PossibleEquations($possibileNumbers, $possibileOperators, $target);
        $gameOutput = new gameOutput($choosenRandNumbers, $result[0], $result[1], $target, $counter);
	}

    function PossibleEquations($possibileNumbers, $possibileOperators, $target)
    {
    	$equation = array();
    	foreach ($possibileNumbers as $i => $outerValue) 
    	{
    		$temp = '';
    		foreach ($possibileNumbers[$i] as $j => $innerValue) 
    		{
    			$randOperator = rand(0,3);
    			if ($j < 5) {
    				$temp = $innerValue . $possibileOperators[1][$randOperator] . $temp;
	    		} else {
	    			$temp .= $innerValue;
	    		}
	    		array_push($equation, $temp);
    		}
    	}
    	return $this->Evaluate($equation, $target);
	}

	function Evaluate($equation, $target)
	{
		$reachedOrClose = array(999,999);
		$duration = microtime(true);
		foreach ($equation as $key => $value) {
			$lastChar = substr($equation[$key], -1);
			if ($lastChar == '+' || $lastChar == '-' || $lastChar == '/' || $lastChar == '*') {
				continue;
			} else {
				$value = eval('return '.$equation[$key].';');
			}
			if ($value < 0 || $value < 101 || $value > 999 || !is_int($value)) {
				continue;
			} elseif ($value == $target) {
				$duration = microtime(true) - $duration;
				echo "executed in $duration seconds\n";
				$reachedOrClose[0] = $equation[$key];
				$reachedOrClose[1] = 0;
				return $reachedOrClose;
			} else {
				$rest = $target - $value; 
				if ($rest < $reachedOrClose[1] && $rest > 0) {
					$reachedOrClose[0] = $equation[$key];
					$reachedOrClose[1] = $rest;
					continue;
				} else {
					continue;
				}
			}
		}
		return $reachedOrClose;
	}
}
?>