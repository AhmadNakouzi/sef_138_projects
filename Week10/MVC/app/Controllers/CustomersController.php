<?php

require_once("config.php");

class CustomersController
{
	private $host = '';
 	private $username = '';
 	private $password = '';
 	private $db_name = '';
 	private $db = '';

 	/**
 	*Initialize all database variables
 	*/
 	public function __construct()
 	{
 		$this->host = HOST;
	 	$this->username = USERNAME;
	 	$this->password = PASSWORD;
	 	$this->db_name = DB_NAME;
 	}
 	
 	/**
 	*Connect to Sakila Database
 	*/
	public function Connect()
 	{
		$this->db = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
 	}

 	/**
 	*Close the database connection
 	*/
 	public function Close()
 	{
 		mysqli_close($this->db);
 	}

 	/**
 	*Retrieve all customers from database 
 	*/
	function index()
	{
		// Check connection
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
		} 

		$customer = "SELECT customer_id, store_id, first_name, last_name, email, address_id, active FROM customer;";
		$result = mysqli_query($this->db, $customer);

		if ($result->num_rows > 0) {
			$customers = [];
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		    	array_push($customers, $row);
		    }
		    $json = json_encode($customers, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
			print_r($json);
		} else {
		    $error->message = '0 records founded';
			$message = json_encode($error);
			print($message);
		}
		$this->Close();
	}

	/**
	*Create a new customer in the database
	*/
	function create()
	{
		// Check connection
		if ($this->Connect()->connect_error) {
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);
    		return;
		} else {
  			if (isset($_POST['first_name']) && isset($_POST['last_name'])
  				&& isset($_POST['email']) && isset($_POST['store_id'])
  				&& isset($_POST['address']) && isset($_POST['active'])) {
  				
  				$store =  $_POST['store_id'];
  				$first_name = $_POST['first_name'];
				$last_name =  $_POST['last_name'];
				$email = $_POST['email'];
				$address = $_POST['address'];
				$active = $_POST['active'];

				//validate that address and store already exists in database
				$address_id = $this->validateAddress($address);
				$store_id = $this->validateStore($store);
				//validate that email doesn't exist because it is unique
				$validate_email = $this->validateEmail($email);
				
				if ($address_id !== null && $store_id !== null && $validate_email === null) {

					$newCustomer = "INSERT INTO customer(store_id, first_name, last_name, email, address_id, active) VALUES('" . $store . "', '" . $first_name . "', '" . $last_name . "', '" . $email . "', '" . $address_id . "', '" . $active . "');";

		  			try {
		  				mysqli_query($this->db, $newCustomer);
		  				print('New customer added');
		  				$this->Close();
		  			} catch (MySQLDuplicateKeyException $e) {
		  				$e->getMessage();
		  			}	
				} else {
					print('Wrong address, store or email entry');	
				}
				
  			} else {
  				print "Incorrect parameters";
  			} 			
	 	}
	}

	/**
	*Update customer in the database
	*/
	function update()
	{
		$request = new Request;
		$url = $request->getUrl();
		$url = $request->splitUrl($url);
		$requestURL = explode("/", $url[1]);

		// Check connection
		if ($this->Connect()->connect_error) {
    		print("Connection failed to database");
    		$error->message = 'Connection failed to database';
			$message = json_encode($error);
			print($message);	
		} else {
			parse_str(file_get_contents("php://input"), $_PATCH);
			$store = '';
			$first_name = '';
			$last_name = '';
			$email = '';
			$address = '';
			$active = '';
			$updatedAttr = [];

			if (isset($requestURL[2])) {
				if (isset($_PATCH['store_id'])) {
					array_push($updatedAttr, 'store_id');
				}
				if (isset($_PATCH['first_name'])) {
					array_push($updatedAttr, 'first_name');
				}
				if (isset($_PATCH['last_name'])) {
					array_push($updatedAttr, 'last_name');
				}
				if (isset($_PATCH['email'])) {
					array_push($updatedAttr, 'email');
				}
				if (isset($_PATCH['address_id'])) {
					array_push($updatedAttr, 'address_id');
				}
				if (isset($_PATCH['active'])) {
					array_push($updatedAttr, 'active');
				}

				$updateCustomer = "UPDATE customer SET ";

				foreach ($updatedAttr as $key => $value) {
					if ($key < sizeof($updatedAttr)-1) {
						if (is_numeric($_PATCH[$value])) {
							$updateCustomer .= $value . "= " . $_PATCH[$value] . ", ";
						} else {
							$updateCustomer .= $value . "= '" . $_PATCH[$value] . "', ";
						}
					} else {
						if (is_numeric($_PATCH[$value])) {
							$updateCustomer .= $value . "= " . $_PATCH[$value]. " WHERE customer_id=$requestURL[2]";
						} else {
							$updateCustomer .= $value . "= '" . $_PATCH[$value]. "' WHERE customer_id=$requestURL[2]";
						}
					}	
				}
				
				try {
					$result = mysqli_query($this->db, $updateCustomer);
					if ($result) {
						$error->status = "200";
						$error->message = 'Customer updated successfully';
						$message = json_encode($error);
						print($message);
					} else {
						$error->message = 'Error updating new customer';
						$message = json_encode($error);
						print($message);
					}
				} catch (Exception $e) {
					$error->message = 'Error updating new customer';
					$message = json_encode($error);
					print($message);	
				}
			} else {
				$error->message = 'You must specify the customer';
				$message = json_encode($error);
				print($message);
			}
		}
	}

	/**
	*Validate if email already exists to prevent user from creating a new customer
	*/
	function validateEmail($email)
	{
		$email = "SELECT email from customer WHERE email = '" . $email . "'";
		$email = mysqli_query($this->db, $email);
		$email = mysqli_fetch_assoc($email);
		
		return $email['email'];
	}

	/**
	*Validates address if exists to get it's ID
	*/
	function validateAddress($address)
	{
		$address_id = "SELECT address_id from address WHERE address = '" . $address . "'";
		$address_id = mysqli_query($this->db, $address_id);
		$address_id = mysqli_fetch_assoc($address_id);
		
		return $address_id['address_id'];
	}

	/**
	*Validates store if exists to get it's ID
	*/
	function validateStore($store)
	{
		$store_id = "SELECT store_id from store WHERE store_id = '" . $store . "'";
		$store_id = mysqli_query($this->db, $store_id);
		$store_id = mysqli_fetch_assoc($store_id);

		return $store_id['store_id'];
	}
}