@extends('layouts.app')
@section('content')
    @if(isset($blogs))
        @foreach($blogs as $blog)
            <div class="mx-auto mt-2 rounded border-secondary border w-50 p-2"
                 @if(isset($blog->id))
                 id="blog{{$blog->id}}"
                    @endif>
                <h2> {{$blog->title}}</h2>
                <a @if(isset($blog->id)) class="otherBlogs btn btn-secondary mx-auto"
                   id="viewBlog-{{$blog->id}}"
                   href="{{route('readMore',['id' => $blog->id])}}"
                        @endif>Read More About {{$blog->title}}
                </a>
            </div>
        @endforeach
        <div>
            {{$blogs->links()}}
        </div>
    @endif
    @if(session('error'))
        <label>{{ session('error') }}</label>
    @endif
@endsection