use sakila;
SELECT ST.store_id AS STORE, MONTH (P.payment_date) AS  MONTH, YEAR (P.payment_date) AS YEAR, FLOOR(SUM(P.amount)) AS Total, FLOOR(AVG(P.amount)) as Average
FROM payment AS P,staff AS S,store AS ST
WHERE P.staff_id = S.staff_id
AND S.store_id = ST.store_id
GROUP BY ST.store_id, YEAR, MONTH
ORDER BY ST.store_id, YEAR DESC;
