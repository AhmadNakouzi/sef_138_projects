<?php
require 'config.php';
 Class SakilaDb
 {
 	private $host = '';
 	private $username = '';
 	private $password = '';
 	private $db_name = '';
 	private $db = '';

 	public function __construct()
 	{
 		$this->host = HOST;
	 	$this->username = USERNAME;
	 	$this->password = PASSWORD;
	 	$this->db_name = DB_NAME;
 	}
 	//Connect to database
 	public function Connect()
 	{
		$this->db = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
 	}
 	//close the database connection
 	public function Close()
 	{
 		mysqli_close($this->db);
 	}
 	//get the movies that are only available in the inventory
 	public function GetMovies()
 	{
 		try
 		{
 			$query = "select distinct f.film_id, title from film as f join inventory as i on f.film_id=i.film_id join rental as r on r.inventory_id = i.inventory_id where return_date<date(now()) order by f.film_id asc;"; 
	 		$moivesToken = mysqli_query($this->db, $query);
	        return $moivesToken;
 		}
 		catch(Exception $e)
 		{
 			echo $this->alert('nothing ok');
 			return; 			
 		}
 	}
 	//get customer id and store id that belongs to given email
 	public function GetEmailInfo($email)
 	{
 		$query = "SELECT customer_id,store_id FROM customer WHERE email='" . $email . "';";
 		if (mysqli_connect_errno())
		{
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
  		else
  		{
	 		$custID = mysqli_query($this->db, $query);
	 		$custID = mysqli_fetch_assoc($custID);
	 		return $custID;
	 	}		
 	}
 	//Return the inventory ID for a specific film in a store
 	public function GetInventoryID($film,$store)
 	{
 		$query = "SELECT inventory_id FROM inventory WHERE store_id='" . $store . "' AND film_id = '" . $film . "' limit 1;";
 		if (mysqli_connect_errno())
		{
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
  		else
  		{
	 		$inventID = mysqli_query($this->db, $query);
	 		$inventID = mysqli_fetch_assoc($inventID);
	 		return $inventID;
	 	}	
 	}
 	//Distribute work on different staffs based on last working staff for a specific customer
 	//ex: if the last rental for a specific customer was staff 1 then we will give the work to another staff
 	public function GetStaffID($custID)
 	{
 		$lastWorkingStaff = "SELECT staff_id from payment WHERE customer_id = '" . $custID . "' ORDER BY payment_id desc limit 1";
 		if ($lastWorkingStaff == 1)
 		{
 			$query = "SELECT staff_id from staff where staff_id <> 1 LIMIT 1";	
 		}
 		else
 		{
 			$query = "SELECT staff_id from staff where staff_id <> 2 LIMIT 1";		
 		}
 		if (mysqli_connect_errno())
		{
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
  		else
  		{
	 		$staffID = mysqli_query($this->db, $query);
	 		$staffID = mysqli_fetch_assoc($staffID);
	 		return $staffID;
	 	}	
 	}
 	public function InsertNewRental($dateFrom, $invID, $custID, $dateTo, $staff)
 	{
 		$query = "INSERT INTO rental(rental_date,inventory_id,customer_id,return_date,staff_id) VALUES('" . $dateFrom . "', '" . $invID['inventory_id'] . "', '" . $custID['customer_id'] . "', '" . $dateTo . "', '" . $staff['staff_id'] . "');";
 		if (mysqli_connect_errno())
		{
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
  		else
  		{
  			mysqli_query($this->db, $query);
	 	}	
 	}
}
?>