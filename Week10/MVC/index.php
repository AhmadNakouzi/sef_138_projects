<?php 

require_once("Request.php");
require_once("routes/api.php");	

$request = new Request;
$url = $request->getUrl();
$url = $request->splitUrl($url);
$requestURL = explode("/", $url[1]);

$routes = new Routes();
$routes = $routes->get();

//checks for route if exists
if(array_key_exists($requestURL[0], $routes)) {
	//checks for CRUD
	if(array_key_exists($requestURL[1], $routes[$requestURL[0]])) {
		//http request type
		if(strtolower($_SERVER['REQUEST_METHOD']) === $routes[$requestURL[0]][$requestURL[1]]['method']) {
			$expected = $routes[$requestURL[0]][$requestURL[1]]['uses'];
			$expected = explode("@", $expected);
			require_once("app/Controllers/".$expected[0].".php");
			$controller = new $expected[0]();
			$function = $expected[1];
			print_r(json_encode($controller->$function()));
		} else {
			$error->message = 'Incorrect http request type';
			$message = json_encode($error);
			print($message);
		}
	} else {
		$error->message = 'Incorrect use of CRUD';
		$message = json_encode($error);
		print($message);
	}
} else {
	$error->message = 'This route does not exists';
	$message = json_encode($error);
	print($message);
}